﻿namespace TaskManagmentApp.Data
{
    public class OperationResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
