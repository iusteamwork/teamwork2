﻿using System;
using System.Collections.Generic;

namespace TaskManagmentApp.Data.Interfaces
{
    public interface IDataStorage<T>
    {
        bool Save(T item);
        bool Delete(Guid id);
        bool Add(T item);
        bool WriteToFile(List<T> itemList);
        //IEnumerable<T> GetData(Predicate<T> predicate);
        IEnumerable<T> GetData();
    }
}
