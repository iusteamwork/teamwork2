﻿using System;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class RepresentUser
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Role { get; set; }

        public DateTime RegistrationDateTime { get; set; }

        public string RegDate
        {
            get { return RegistrationDateTime.ToString("dd MMMM yyyy"); }
        }


    }
}
