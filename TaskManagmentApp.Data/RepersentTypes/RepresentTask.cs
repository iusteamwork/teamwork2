﻿using System;
using System.Collections.Generic;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class RepresentTask: Tasks
    {
        public string ProjectName { get; set; }
        public string ManagerName { get; set; }
        public string ImplementationIconSource { get; set; }
        public string SelectedValue { get; set; }
        public IEnumerable<string> UserList { get; set; }
        public string DeadLineDate
        {
            get { return DeadLine.ToString("dd MMMM yyyy"); }
        }
        public string CreateDateFormat
        {
            get { return CreateDate.ToString("dd MMMM yyyy"); }
        }
        public string EvaluationHours
        {
            get { return EvaluationTime.ToString("H:mm:ss"); }
        }

        public string ElapsedHours
        {
            get { return ElapsedTime.ToString("H:mm:ss"); }
        }

    }
}
