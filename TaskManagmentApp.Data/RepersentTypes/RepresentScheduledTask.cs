﻿using System;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class RepresentScheduledTask:ScheduledTasks
    {
        public string Login { get; set; }
        public Guid Project { get; set; }
        public string ProjectName { get; set; }
        public byte ImplementationStatus { get; set; }
        public string ImplementationIconSource { get; set; }
        public byte PlannedoOut { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public Guid Manager { get; set; }
        public string ManagerLogin { get; set; }
        public DateTime ElapsedTime { get; set; }
        public DateTime EvaluationTime { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public WorkingTimeClass WorkingTime { get; set; }

        public DateTime CreateDateTime { get; set; }
        public string DeadLineDate
        {
            get { return DeadLine.ToString("dd MMMM yyyy"); }
        }
        public string EvaluationDays
        {
            get { return EvaluationTime.ToString("H:mm:ss"); }
        }

        public string ElapsedTimeFormat
        {
            get { return ElapsedTime.ToString("H:mm:ss"); }
        }

        public string StartTime
        {
            get { return DateStart.ToString("H:mm:ss"); }
        }
        public string EndTime
        {
            get { return DateEnd.ToString("H:mm:ss"); }
        }


    }
}
