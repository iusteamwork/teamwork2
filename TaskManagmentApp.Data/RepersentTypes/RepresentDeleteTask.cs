﻿using System;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class RepresentDeleteTask : Tasks
    {
        public DateTime TimeDeleteTask { get; set; }
        public string ProjectName { get; set; }
        public string ManagerName { get; set; }
        public string ImplementationIconSource { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime DateStart { get; set; }
    }
}
