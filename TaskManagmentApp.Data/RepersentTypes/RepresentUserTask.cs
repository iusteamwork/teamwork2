﻿using System;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class RepresentUserTask:UserTasks
    {
        public string Login { get; set; }
        public Guid Project { get; set; }
        public string ProjectName { get; set; }
        public byte ImplementationStatus { get; set; }
        public byte PlannedOut { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public Guid Manager { get; set; }
        public string ManagerLogin { get; set; }
        public string ImplementationIconSource { get; set; }
        public DateTime EvaluationTime { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime ElapsedTime { get; set; }

        public string DeadLineDate
        {
            get { return DeadLine.ToString("dd MMMM yyyy"); }
        }

        public string EvaluationHours
        {
            get { return EvaluationTime.ToString("H:mm:ss"); }
        }

    }
}
