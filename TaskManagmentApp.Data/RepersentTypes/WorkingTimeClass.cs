﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagmentApp.Data.RepersentTypes
{
    public class WorkingTimeClass
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public WorkingTimeClass()
        {

        }
        public WorkingTimeClass(DateTime begin, DateTime end)
        {
            StartDate = begin;
            EndDate = end;
        }
    }
}
