﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class ScheduledTasks
    {
        public Guid User { get; set; }
        public Guid Task { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

    }
}
