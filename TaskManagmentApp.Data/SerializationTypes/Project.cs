﻿using System;
using System.Xml.Serialization;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class Project
    {
        [XmlAttribute]
        public Guid Id {get; set;}
        public string Name { get; set; }
        public DateTime Date { get; set; }
    }
}
