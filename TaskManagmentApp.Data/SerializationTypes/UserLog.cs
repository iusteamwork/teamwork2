﻿using System;
using System.Xml.Serialization;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class UserLog
    {
        [XmlAttribute]
        public Guid UserId { get; set; }
        [XmlAttribute]
        public string User { get; set; }
        public string Task { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public string Action { get; set; }

        public string DateForamt
        {
            get { return Date.ToString("dd MMMM yyyy"); }
        }
    }
}
