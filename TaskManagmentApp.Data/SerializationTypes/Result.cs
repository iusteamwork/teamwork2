﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagmentApp.Data.SerializationTypes
{
    //Data model from authorization response
    public class Result
    {
        public Guid User { get; set; }
        public byte Role { get; set; }
    }
}
