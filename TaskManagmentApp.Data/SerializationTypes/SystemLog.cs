﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class SystemLog
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
    }
}
