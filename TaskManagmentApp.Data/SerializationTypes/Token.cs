﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class Token
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string WindowsIdentifier { get; set; }
        public bool IsRemember { get; set; }
    }
}
