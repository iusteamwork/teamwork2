﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class DeletedTasks:Tasks
    {
        public Guid User { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime DateStart { get; set; }
    }
}
