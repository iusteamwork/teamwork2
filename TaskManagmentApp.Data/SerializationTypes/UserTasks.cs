﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class UserTasks
    {
        public Guid User { get; set; }
        public Guid Task { get; set; }

    }
}
