﻿using System;
using System.Xml.Serialization;

namespace TaskManagmentApp.Data.SerializationTypes
{
    public class Tasks
    {
        [XmlAttribute]
        public Guid Id { get; set; }
        [XmlAttribute]
        public Guid Project { get; set; }
        public byte ImplementationStatus { get; set; }
        public byte PlannedOut { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public Guid Manager { get; set; }
        public DateTime EvaluationTime { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime ElapsedTime { get; set; }
        public DateTime TimeDeleteTask { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
