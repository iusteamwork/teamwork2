﻿using System;

namespace TaskManagmentApp.Data.SerializationTypes
{
   public class User
    {
        public Guid Id { get; set; }
        public string Login {get; set;}
        public byte[] Password { get; set; }
        public byte[] Hash { get; set; }
        public byte Role { get; set; }

        public DateTime RegistrationDateTime { get; set; }
    }
}
