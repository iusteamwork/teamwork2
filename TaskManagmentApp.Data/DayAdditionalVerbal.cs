﻿namespace TaskManagmentApp.Data
{
    public static class DayAdditionalVerbal
    {
        public static string Verbal(int day)
        {
            if (day == 0) return "сегодня";
            if (day < 5 && day > 1) return day+" дня";
            if (day < 21 && day > 4) return day+" дней";
            if (day < 26 && day > 21) return day+" дня5";
            return "";
        }
    }
}
