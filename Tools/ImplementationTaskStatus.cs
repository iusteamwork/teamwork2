﻿using System;

namespace Tools
{
    class ImplementationTaskStatus
    {
        public static int GetStatus(DateTime deadline, DateTime begin)
        {
            Console.WriteLine(DateTime.Now.Subtract(begin).TotalHours);

            var diff = deadline.Subtract(begin).TotalHours / 3;
            if (DateTime.Now.Subtract(begin).TotalHours < diff)
            {
                return 1;
            }
            if (DateTime.Now.Subtract(begin).TotalHours < diff * 2) return 2;
            return 3;
        }
    }
}
