﻿using System.Collections.Generic;
using System.Linq;

namespace Tools
{
    public class Paginator<T>
    {
        private readonly List<T> _list;
        private readonly int _row;
        public int PageCount { get; private set; }
        public Paginator(List<T> list, int row)
        {
            _list = list;
            _row = row;
            PageCount = (_list.Count / _row) + (_list.Count % _row != 0 ? 1 : 0);
        }
        public List<T> GetPageData(int page)
        {
            if (page < 0 || page > PageCount)
                return null;
            return _list.Skip((page - 1) * _row).ToList().Take(_row).ToList();
        }
    }
}
