﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using AppSerialization.Storage;
using System;
using System.Linq;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class UserService
    {
        private readonly FactoryService _storage;

        public UserService(FactoryService storage)
        {
            _storage = storage;
        }

        /// <summary>
        /// Return reperesent user list
        /// </summary>
        /// <returns></returns>
        public List<RepresentUser> GetUsersList()
        {
            var userStorage = _storage.GetUserRepository();
            return (from u in userStorage.GetData() select new RepresentUser
            {
                Id = u.Id,
                Login = u.Login,
                RegistrationDateTime = u.RegistrationDateTime,
                Role = u.Role==0? "Пользователь" : "Админинстратор"
            }).ToList();
        }

        
        /// <summary>
        /// Change role for selected user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool ChangeUserRole(RepresentUser user)
        {
            if (user == null) return false;
            var userStorage = _storage.GetUserRepository();
            var userModel = userStorage.GetData().FirstOrDefault(u => u.Id.Equals(user.Id));
            if (userModel == null) return false;
            if (user.Role == "0") userModel.Role = 1;
            if (user.Role == "1") userModel.Role = 0;
            return userStorage.Save(userModel);
        }

        /// <summary>
        /// Authorize user with solt
        /// </summary>
        /// <param name="userLogin"></param>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        public Result AuthorizeUser(string userLogin, string userPassword)
        {
            if (String.IsNullOrEmpty(userLogin.Trim()) || String.IsNullOrEmpty(userPassword.Trim())) return null;
            var user = GetUserByLogin(userLogin);
            if (user == null) return null;
            var cryptPassword = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(userPassword.Trim()));
            var hashPassword = SHA256.Create().ComputeHash(cryptPassword);
            if (user.Password.ToString().Equals(cryptPassword.ToString()) && user.Hash.ToString().Equals(hashPassword.ToString())) return new Result {User = user.Id, Role = user.Role};
            return null;
        }

        /// <summary>
        /// Registrate new user in system
        /// </summary>
        /// <param name="userLogin"></param>
        /// <param name="userPassword"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public User RegistrateUser(string userLogin, string userPassword,byte userRole)
        {
            if (String.IsNullOrEmpty(userLogin.Trim()) || String.IsNullOrEmpty(userPassword.Trim())) return null;
            var user = GetUserByLogin(userLogin);
            if (user != null) return null;
            var cryptPassword = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(userPassword.Trim()));
            var hashPassword = SHA256.Create().ComputeHash(cryptPassword);
            var userStorgge=_storage.GetUserRepository();
            var userModel = new User
            {
                Id = Guid.NewGuid(),
                Login = userLogin,
                RegistrationDateTime = DateTime.Now,
                Password = cryptPassword,
                Hash = hashPassword,
                Role = userRole
            };
            if (userStorgge.Add(userModel)) return userModel;
            return null;
        }


        /// <summary>
        /// Get user by user login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public User GetUserByLogin(string login)
        {
            if (String.IsNullOrEmpty(login)) return null;
            var users = _storage.GetUserRepository().GetData().ToList();
            if (!users.Any()) return null;
            var user = users.Where(u => u.Login == login).ToList();
            return user.Any() ? user.FirstOrDefault() : null;
        }

       /// <summary>
       /// Get user id by his login
       /// </summary>
       /// <param name="login"></param>
       /// <returns></returns>
        public Guid GetUserIdByLogin(string login)
        {
            var user = GetUserByLogin(login);
            return user != null ? user.Id : Guid.Empty;
        }

        /// <summary>
        /// Return user by userId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetRecord(Guid id)
        {
            var user = _storage.GetUserRepository().GetData().ToList();
            return user.Any() ? user.Single() : null;
        }
        
        /// <summary>
        /// Return user user role by userId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public byte? GetUserRole(Guid id)
        {
            var user = _storage.GetUserRepository().GetData().Where(u=>u.Id.Equals(id)).ToList();
            if (user.Any()) return user.Single().Role;
            return null;
        }

        /// <summary>
        /// Delete users from all tables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteFromAll(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var userDataStorage = _storage.GetUserRepository();
            var userList = userDataStorage.GetData().ToList();
            // Checking the existence of user
            var userItem = userList.FirstOrDefault(t=>t.Id.Equals(id));
            if (userItem == null) return false;
            var userTaskDataStorage = new UserTaskRepository();
            var scheduledTaskService = new ScheduledTaskService(_storage);        
            //Check record exist in another files
            if (userTaskDataStorage.GetData().Any(t => t.User.Equals(id)) || scheduledTaskService.GetRecord(id) != null)
            {
                throw new InvalidOperationException("Record exist");
            }
            //Delete user
            if (!userDataStorage.WriteToFile(userList)) return false;
            var storageForDeleted = new UserRepository();
            return storageForDeleted.Delete(userItem.Id);
        }

        public User GetUser(Guid id)
        {
            var user = _storage.GetUserRepository().GetData().Where(c => c.Id == id).First();
            return user;
        }
    }
}
