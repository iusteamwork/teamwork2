﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class TokenService
    {
        private readonly FactoryService _storage;

        // Interfaces used to determine the type of storage (database, serialization)
        public TokenService(FactoryService storage)
        {
            if (storage == null) throw new ArgumentNullException("storage");
            _storage = storage;
        }

        public bool IsRememberUser(Guid userId)
        {
            if(userId!=Guid.Empty)
            {
                var storage = _storage.GetTokenRepository();
                var windowsIdentifier = WindowsIdentity.GetCurrent().Token;
                return storage.GetData().FirstOrDefault(u => u.UserId.Equals(userId) && u.WindowsIdentifier.Equals(windowsIdentifier)).IsRemember;
            }
            return false;
        }

        public IEnumerable<Guid> GetOperationSystemUsers()
        {
            var storage = _storage.GetTokenRepository().GetData();
            var windowsIdentifier = WindowsIdentity.GetCurrent().Token;
            return storage.Where(u => u.WindowsIdentifier.Equals(windowsIdentifier)).Select(i => i.UserId);
        }

        public bool AddToken(Guid userId)
        {
            if (userId != Guid.Empty)
            {
                var storage = _storage.GetTokenRepository();
                var windowsIdentifier = WindowsIdentity.GetCurrent().User.Value;
                return storage.Add(new Token
                {
                    Id= Guid.NewGuid(),
                    IsRemember = true,
                    UserId = userId,
                    WindowsIdentifier =windowsIdentifier
                });
            }
            return false;
        }

        public IEnumerable<string> GetUserOsIdentifiers(Guid userId)
        {
            if (userId != Guid.Empty)
            {
                var storage = _storage.GetTokenRepository().GetData();
                return storage.Where(u => u.UserId.Equals(userId)).Select(i => i.WindowsIdentifier);
            }
            return Enumerable.Empty<string>();
        }

    }
}
