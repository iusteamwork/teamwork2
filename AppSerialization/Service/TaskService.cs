﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppSerialization.Storage;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class TaskService
    {
        private readonly FactoryService _storage;

        // Interfaces used to determine the type of storage (database, serialization)
        public TaskService(FactoryService storage)
        {
            if (storage == null) throw new ArgumentNullException("storage");
            _storage = storage;
        }

        /// <summary>
        /// Gets a list of tasks assigned to the project.
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public List<RepresentTask> GetProjectTasks(Guid projectId)
        {
            if (projectId == Guid.Empty) return null;
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           where t.Project.Equals(projectId)
                           select new RepresentTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ElapsedTime = t.ElapsedTime,
                               UserList = GetUsersForTasks(t.Id).Select(i=>i.Login),
                               CreateDate = t.CreateDate,
                               TimeDeleteTask = t.TimeDeleteTask,
                               SelectedValue = GetUsersForTasks(t.Id).Select(i => i.Login).FirstOrDefault()
                           };
            return taskList.ToList();
        }


        /// <summary>
        /// Return record id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Tasks GetRecord(Guid id)
        {
            if (id == Guid.Empty) return null;
            var taskList = _storage.GetTaskRepository().GetData();
            return taskList.FirstOrDefault(t => t.Id.Equals(id));
        }

        /// <summary>
        /// Get all users for selected task
        /// </summary>
        /// <param name="taskGuid"></param>
        /// <returns></returns>
        public List<RepresentUser> GetUsersForTasks(Guid taskGuid)
        {
            if (taskGuid == Guid.Empty) return null;
            //Get users for selected task
            var taskList = _storage.GetUserTaskRepository().GetData().Where(t => t.Task.Equals(taskGuid)).ToList();
            if (!taskList.Any()) return new List<RepresentUser>();
            var userList = _storage.GetUserRepository().GetData().ToList();
            return (from task in taskList
                join user in userList on task.User equals user.Id
                select new RepresentUser
                {
                    Id = user.Id,
                    Login = user.Login,
                    RegistrationDateTime = user.RegistrationDateTime,
                    Role = user.Role == 0 ? "Пользователь" : "Админ"
                }).ToList();
        }

        /// <summary>
        /// Deletes the task assigned to the project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public bool DeleteProjectTasks(Guid projectId)
        {
            if (projectId == Guid.Empty) return false;
            var taskDataStorage = _storage.GetTaskRepository();
            var taskList = taskDataStorage.GetData().ToList();
            var projectTasks = taskList.Where(t => t.Project.Equals(projectId)).ToList();
            foreach (var task in projectTasks)
            {
                taskList.Remove(task);
            }
            return taskDataStorage.WriteToFile(taskList);
        }

        /// <summary>
        /// Remove task without check
        /// </summary>
        /// <returns></returns>
        public bool RemoveCompletely(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var taskDataStorage = _storage.GetTaskRepository();
            var taskList = taskDataStorage.GetData().ToList();
            // Checking the existence of task
            var taskItem = taskList.FirstOrDefault(t => t.Id.Equals(id));
            if (taskItem == null) return false;
            var userTaskDataStorage = _storage.GetUserTaskRepository();
            var scheduledTaskService = _storage.GetScheduledTasksRepository();
            if (taskDataStorage.Delete(id) && userTaskDataStorage.Delete(id) && scheduledTaskService.Delete(id)) return true;
            return false;
        }


        /// <summary>
        /// Delete task from all tables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteFromAll(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var taskDataStorage = _storage.GetTaskRepository();
            var taskList = taskDataStorage.GetData().ToList();
            // Checking the existence of task
            var taskItem = taskList.FirstOrDefault(t => t.Id.Equals(id));
            if (taskItem == null) return false;
            var userTaskDataStorage = new UserTaskRepository();
            var scheduledTaskService = new ScheduledTaskService(_storage);
            //Check record exist in another files
            if (userTaskDataStorage.GetData().Any(t => t.Task.Equals(id)) || scheduledTaskService.GetRecord(id) != null)
            {
                throw new InvalidOperationException("Record exist");
            }
            //Save data
            if (!taskDataStorage.WriteToFile(taskList)) return false;
            var storageForDeleted = new DeletedTasksRepository();
            return storageForDeleted.Add(taskItem as DeletedTasks);
        }

        /// <summary>
        /// Get list of represent tasks
        /// </summary>
        /// <returns></returns>
        public List<RepresentTask> GetTasks()
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           select new RepresentTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ManagerName = us.Login,
                               ElapsedTime = t.ElapsedTime,
                               CreateDate = t.CreateDate
                           };
            return taskList.ToList();
        }

        /// <summary>
        /// Get all tasks for project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public List<RepresentTask> GetTasks(Guid projectId)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           where ps.Id.Equals(projectId)
                           select new RepresentTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ManagerName = us.Login,
                               ElapsedTime = t.ElapsedTime,
                               CreateDate = t.CreateDate
                               
                           };
            return taskList.ToList();
        }

        public List<RepresentTask> GetTasks(Guid projectId, Guid userGuid)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var userTasks = _storage.GetUserTaskRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           join ut in userTasks on t.Id equals ut.Task
                           where ut.User.Equals(userGuid) && ps.Id.Equals(projectId)
                           select new RepresentTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ManagerName = us.Login,
                               ElapsedTime = t.ElapsedTime,
                               CreateDate = t.CreateDate

                           };
            return taskList.ToList();
        }
        /// <summary>
        /// Get represent tasks for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<RepresentTask> GetUserTasks(Guid userId)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var userTasks = _storage.GetUserTaskRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           join ut in userTasks on t.Id equals ut.Task
                           where ut.User.Equals(userId)
                           select new RepresentTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ManagerName = us.Login,
                               ElapsedTime = t.ElapsedTime,
                               CreateDate = t.CreateDate
                                
                           };
            return taskList.ToList();
        }

        public IEnumerator<Tasks> GetEnumerator()
        {
            foreach (object task in _storage.GetTaskRepository().GetData())
                yield return (Tasks)task;
        }

        /// <summary>
        /// Make selected task is complete
        /// </summary>
        /// <param name="representTask"></param>
        public bool CompleteTask(Guid taskGuid)
        {
            if (taskGuid==Guid.Empty) return false;
            var taskStorage = _storage.GetTaskRepository();
            var task = taskStorage.GetData().First(t => t.Id.Equals(taskGuid));
            if (task != null)
            {
                task.ImplementationStatus = (byte) (task.ImplementationStatus == 0 ? 1 : 0);
                return taskStorage.Save(task);
            }
            return false;
        }
    }
}
