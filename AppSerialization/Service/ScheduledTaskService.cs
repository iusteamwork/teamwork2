﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class ScheduledTaskService
    {    
        private readonly FactoryService _storage;

        //  Interfaces used to determine the type of storage (database, serialization)
        public ScheduledTaskService(FactoryService storage)
        {
            if (storage == null) throw new ArgumentNullException("storage");
            _storage = storage; 
            
        }
        
        /// <summary>
        /// Returns a list of distributed tasks
        /// </summary>
        /// <returns></returns>
        public List<RepresentScheduledTask> GetTasks()
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var scheduled = _storage.GetScheduledTasksRepository().GetData();
            
            var userService = new UserTaskService(_storage);
            var taskList = from sc in scheduled
                           join t in task on sc.Task equals t.Id
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           //join sc in scheduled on us.Id equals sc.User

                           select new RepresentScheduledTask
                           {
                               About=t.About,
                               DeadLine=t.DeadLine,
                               ElapsedTime=t.ElapsedTime,
                               EvaluationTime=t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus=t.ImplementationStatus,
                               Login=us.Login,
                               Manager=t.Manager,
                               ManagerLogin=userService.GetUserLogin(t.Manager),
                               Name=t.Name,
                               PlannedoOut=t.PlannedOut,
                               Project=t.Project,
                               ProjectName=ps.Name,
                               Task=t.Id,
                               User=us.Id,
                               DateStart = sc.DateStart,
                               DateEnd = sc.DateEnd,
                               CreateDateTime = t.CreateDate,
                               WorkingTime = new WorkingTimeClass { StartDate = sc.DateStart, EndDate = sc.DateEnd },
                           };
            return taskList.ToList();
        }

        /// <summary>
        /// Returns a list of distributed tasks of Project
        /// </summary>
        /// <returns></returns>
        public List<RepresentScheduledTask> GetTasks(Guid IdUser, Guid IdProdject)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var scheduled = _storage.GetScheduledTasksRepository().GetData();

            var userService = new UserTaskService(_storage);
            var taskList = from sc in scheduled
                           join t in task on sc.Task equals t.Id
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           where ps.Id.Equals(IdProdject)
                           where sc.User.Equals(IdUser)
                           select new RepresentScheduledTask
                           {
                               About = t.About,
                               DeadLine = t.DeadLine,
                               ElapsedTime = t.ElapsedTime,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Login = us.Login,
                               Manager = t.Manager,
                               ManagerLogin = userService.GetUserLogin(t.Manager),
                               Name = t.Name,
                               PlannedoOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               Task = t.Id,
                               User = us.Id,
                               DateStart = sc.DateStart,
                               DateEnd = sc.DateEnd,
                               CreateDateTime = t.CreateDate,
                               WorkingTime = new WorkingTimeClass { StartDate = sc.DateStart, EndDate = sc.DateEnd },
                           };
            return taskList.ToList();
        }


        /// <summary>
        /// Returns a list of distributed tasks of User
        /// </summary>
        /// <returns></returns>
        public List<RepresentScheduledTask> GetTasksByUser(Guid item)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var scheduled = _storage.GetScheduledTasksRepository().GetData();

            var userService = new UserTaskService(_storage);
            var taskList = from sc in scheduled
                           join t in task on sc.Task equals t.Id
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           //join sc in scheduled on us.Id equals sc.User
                           //where ps.Id.Equals(item)
                           where sc.User.Equals(item)

                           select new RepresentScheduledTask
                           {
                               About = t.About,
                               DeadLine = t.DeadLine,
                               ElapsedTime = t.ElapsedTime,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Login = us.Login,
                               Manager = t.Manager,
                               ManagerLogin = userService.GetUserLogin(t.Manager),
                               Name = t.Name,
                               PlannedoOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               Task = t.Id,
                               User = us.Id,
                               DateStart = sc.DateStart,
                               DateEnd = sc.DateEnd,
                               CreateDateTime = t.CreateDate,
                               WorkingTime = new WorkingTimeClass { StartDate = sc.DateStart, EndDate = sc.DateEnd },
                           };
            return taskList.ToList();
        }


        /// <summary>
        /// Deletes a scheduled task
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool DeleteScheduleTask(ScheduledTasks item)
        {
            if (item == null) return false;
            if (item.Task == Guid.Empty || item.User == Guid.Empty) return false;
            var scheduledTasksDataStorage = _storage.GetScheduledTasksRepository();
            var scheduledTasks = scheduledTasksDataStorage.GetData().ToList();
            var scheduleTaskItem = scheduledTasks.FirstOrDefault(st => st.User.Equals(item.User) && st.Task.Equals(item.Task));
            if (scheduleTaskItem == null) return false;
            //Сохранить и серализировать
            scheduledTasks.Remove(scheduleTaskItem);
            return scheduledTasksDataStorage.WriteToFile(scheduledTasks);
        }
        /// <summary>
        /// Final deletes tasks
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool DeletePermanently(Guid taskId)
        {
            if (taskId == Guid.Empty) return false;
            var scheduledTasksStorage = _storage.GetScheduledTasksRepository();
            var tasks = scheduledTasksStorage.GetData().ToList();
            var taskForDelete = tasks.FirstOrDefault(t => t.Task.Equals(taskId));
            if (taskForDelete == null) return false;
            return tasks.Remove(taskForDelete) && scheduledTasksStorage.WriteToFile(tasks);
        }

        /// <summary>
        /// Returns one record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ScheduledTasks GetRecord(Guid id)
        {
            if (id == Guid.Empty) return null;
            var scheduledTasks = _storage.GetScheduledTasksRepository().GetData();
            return scheduledTasks.FirstOrDefault(st => st.Task.Equals(id));
        }

        /// <summary>
        /// Get user scheduled tasks
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<ScheduledTasks> GetUserScheduledTasks(Guid userId)
        {
            if (userId == Guid.Empty) return new List<ScheduledTasks>();
            var scheduledTasks = _storage.GetScheduledTasksRepository().GetData();
            return scheduledTasks.Where(st => st.User.Equals(userId)).ToList();
        }
    }
}