﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class UserTaskService
    {
        private readonly FactoryService _storage;
        private readonly XmlSerialize<User> _xmlSerializer = new XmlSerialize<User>("User.xml");

        public UserTaskService(FactoryService storage)
        {
            _storage = storage;

        }

        public List<RepresentUserTask> GetUserTasks()
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           select new RepresentUserTask
                           {
                               Login = us.Login,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ImplementationStatus = t.ImplementationStatus,
                               PlannedOut = t.PlannedOut,
                               Name = t.Name,
                               About = t.About,
                               Manager = t.Manager,
                               ManagerLogin = us.Login,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               EvaluationTime = t.EvaluationTime,
                               DeadLine = t.DeadLine,
                               ElapsedTime = t.ElapsedTime
                           };
            return taskList.ToList();
        }

        public List<RepresentUserTask> GetUserTasks(Guid userGuid)
        {
            if (userGuid == Guid.Empty) return new List<RepresentUserTask>();
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var userTasks = _storage.GetUserTaskRepository().GetData();
            var taskList = from t in task
                join ps in project on t.Project equals ps.Id
                join us in users on t.Manager equals us.Id
                join ut in userTasks on t.Id equals ut.Task
                where ut.User.Equals(userGuid)
                select new RepresentUserTask
                {
                    Task = t.Id,
                    User = userGuid,
                    Login = us.Login,
                    Project = t.Project,
                    ProjectName = ps.Name,
                    ImplementationStatus = t.ImplementationStatus,
                    PlannedOut = t.PlannedOut,
                    Name = t.Name,
                    About = t.About,
                    Manager = t.Manager,
                    ManagerLogin = us.Login,
                    ImplementationIconSource =
                        t.ImplementationStatus == 0 ? "../../Resources/time.png" : "../../Resources/complete.png",
                    EvaluationTime = t.EvaluationTime,
                    DeadLine = t.DeadLine,
                    ElapsedTime = t.ElapsedTime
                };
            return taskList.ToList();
        }

        public List<RepresentUserTask> GetUserTasks(Guid userGuid, Guid projectGuid)
        {
            if (userGuid == Guid.Empty && projectGuid== Guid.Empty) return new List<RepresentUserTask>();
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var userTasks = _storage.GetUserTaskRepository().GetData();
            var taskList = from t in task
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           join ut in userTasks on t.Id equals ut.Task
                           where ut.User.Equals(userGuid) && ps.Id.Equals(projectGuid)
                           select new RepresentUserTask
                           {
                               Task = t.Id,
                               User = userGuid,
                               Login = us.Login,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ImplementationStatus = t.ImplementationStatus,
                               PlannedOut = t.PlannedOut,
                               Name = t.Name,
                               About = t.About,
                               Manager = t.Manager,
                               ManagerLogin = us.Login,
                               ImplementationIconSource =
                                   t.ImplementationStatus == 0 ? "../../Resources/time.png" : "../../Resources/complete.png",
                               EvaluationTime = t.EvaluationTime,
                               DeadLine = t.DeadLine,
                               ElapsedTime = t.ElapsedTime
                           };
            return taskList.ToList();
        }

        /// <summary>
        /// Remove user task
        /// </summary>
        /// <param name="userTask"></param>
        /// <returns></returns>
        public bool DeleteUserTask(UserTasks userTask)
        {
            if (userTask == null) return false;
            if (userTask.User == Guid.Empty || userTask.Task == Guid.Empty) return false;
            var userTaskDataStorage = _storage.GetUserTaskRepository();
            var userTasks = userTaskDataStorage.GetData().ToList();
            var temp = userTasks.FirstOrDefault(i => i.Task.Equals(userTask.Task) && i.User.Equals(userTask.User));
            if (userTasks.Remove(temp))
            {
                //Kostil :D
                return userTaskDataStorage.WriteToFile(userTasks);
            }
            return false;
        }

        /// <summary>
        /// Get user login by id
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public string GetUserLogin(Guid manager)
        {
            var userTask = _xmlSerializer.Deserialize();
            var checkUser = userTask.FirstOrDefault(u => u.Id.Equals(manager));
            return checkUser != null ? checkUser.Login : null;
        }

        //public bool UnbindUserTask(Guid task,Guid user)
        //{
        //    if (task != Guid.Empty && user != Guid.Empty)
        //    {
        //        var userTasksDataStorage = _storage.GetUserTaskRepository();
        //        var userTasks = userTasksDataStorage.GetData().ToList().FirstOrDefault(ut => ut.User.Equals(user) && ut.Task.Equals(task));
        //        if (userTasks != null)
        //        {
        //           // userTasksDataStorage.
        //        }
        //    }
        //}
    }
}
