﻿using AppSerialization.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Service
{
    public class ProjectService
    {
        private readonly FactoryService _storage;

        public ProjectService(FactoryService storage)
        {
            _storage = storage;
        }

        public List<string> GetTextualProjectList()
        {
            var projectList = _storage.GetProjectRepository().GetData().Select(p=>p.Name);
            return projectList.ToList();
        }

        public List<string> TextualProjectListWithTaskCount()
        {
            var projects = _storage.GetProjectRepository().GetData();
            var tasks = _storage.GetTaskRepository().GetData();
            var projectList = from p in projects
                select new {Project=p, Count=tasks.Count(tp=>tp.Project.Equals(p.Id))};
            return projectList.Select(project => String.Format("({0}) - {1}", project.Count, project.Project.Name)).ToList();
        }



        public Guid GetProjectIdByName(string projectName)
        {
            if (string.IsNullOrEmpty(projectName)) return Guid.Empty;
            var projectList = _storage.GetProjectRepository().GetData();
            var project = projectList.FirstOrDefault(i => i.Name.Equals(projectName.Trim()));
            return project != null ? project.Id : Guid.Empty;
        }

        public Project GetRecord(Guid id)
        {
            if (id == Guid.Empty) return null;
            var projectList = _storage.GetProjectRepository().GetData();
            return projectList.FirstOrDefault(t => t.Id.Equals(id));
        }

        /// <summary>
        /// Remove project and his task without check
        /// </summary>
        /// <returns></returns>
        public bool RemoveCompletely(Guid projectId)
        {
            if (projectId == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            if (!_storage.GetProjectRepository().Delete(projectId)) return false;
            var taskService = new TaskService(_storage);
            var projectTasks=taskService.GetProjectTasks(projectId);
            foreach (var task in projectTasks)
            {
                taskService.RemoveCompletely(task.Id);
            }
            return true;
        }

        /// <summary>
        /// Delete project from all tables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteFromAll(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var projectDataStorage = _storage.GetProjectRepository();
            var projectList = projectDataStorage.GetData();
            // Checking the existence of project
            var projectItem = projectList.FirstOrDefault(t=>t.Id.Equals(id));
            if (projectItem == null) return false;
            //Check record exist in another files
            var tasksDataStorage = new TasksRepository();
           if (tasksDataStorage.GetData().Any(t=>t.Project.Equals(id)))
           {
               throw new InvalidOperationException("Record exist");
           }
           if (!projectDataStorage.WriteToFile(projectList.ToList())) return false;
            //delete project
            var storageForDeleted = new ProjectRepository();
            return storageForDeleted.Delete(projectItem.Id);
        }

        public List<string> GetTextualProjectList(IEnumerable<IGrouping<Guid,RepresentTask>> task)
        {
            var projectList = _storage.GetProjectRepository().GetData();
            var projectNames = new List<string>();
            projectNames.Add("Все проекты");
            foreach(var item in task)
            {
                var project=projectList.FirstOrDefault(p => p.Id.Equals(item.Key));
                if (project != null) projectNames.Add(project.Name);
            }
            return projectNames;
        }
    }
}
