﻿using System;
using System.Linq;
using TaskManagmentApp.Data.RepersentTypes;
using System.Collections.Generic;

namespace AppSerialization.Service
{
    public class DeletedTaskService
    {
        private readonly FactoryService _storage;
        public DeletedTaskService(FactoryService storage)
        {
            _storage = storage;
        }

        /// <summary>
        /// Восстанавливает удаленную задачу по GUID
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool Restore(Guid taskId)
        {
            if (taskId == Guid.Empty) return false;
            var deletedTasksDataStorage = _storage.GetDeletedTasksRepository();
            var deletedTasks = deletedTasksDataStorage.GetData().ToList();
            var item = deletedTasks.FirstOrDefault(c => c.Id.Equals(taskId));
            if (item == null) return false;
            deletedTasks.Remove(item);
            return deletedTasksDataStorage.WriteToFile(deletedTasks);
        }

        public List<RepresentDeleteTask> GetTasks(Guid userId)
        {
            var task = _storage.GetTaskRepository().GetData();
            var project = _storage.GetProjectRepository().GetData();
            var users = _storage.GetUserRepository().GetData();
            var deleted = _storage.GetDeletedTasksRepository().GetData();
            var taskList = from d in deleted
                           join t in task on d.Id equals t.Id
                           join ps in project on t.Project equals ps.Id
                           join us in users on t.Manager equals us.Id
                           where d.User.Equals(userId)
                           select new RepresentDeleteTask
                           {
                               Id = t.Id,
                               About = t.About,
                               DeadLine = t.DeadLine,
                               EvaluationTime = t.EvaluationTime,
                               ImplementationIconSource = t.ImplementationStatus == 0 ? "Resources/time.png" : "Resources/complete.png",
                               ImplementationStatus = t.ImplementationStatus,
                               Manager = t.Manager,
                               Name = t.Name,
                               PlannedOut = t.PlannedOut,
                               Project = t.Project,
                               ProjectName = ps.Name,
                               ManagerName = us.Login,
                               ElapsedTime = t.ElapsedTime,
                               TimeDeleteTask = d.TimeDeleteTask,
                               CreateDate = t.CreateDate
                           };
            return taskList.ToList();
        }


        /// <summary>
        /// Окончательное удаление задачи
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>

        public bool DeletePermanently(Guid taskId)
        {
            if (taskId == Guid.Empty) return false;
            var deletedTasksStorage = _storage.GetDeletedTasksRepository();
            var tasks = deletedTasksStorage.GetData().ToList();
            var taskForDelete = tasks.FirstOrDefault(t => t.Id.Equals(taskId));
            if (taskForDelete == null) return false;
            return tasks.Remove(taskForDelete) && deletedTasksStorage.WriteToFile(tasks);
        }
    }
}
