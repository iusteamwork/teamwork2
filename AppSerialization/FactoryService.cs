﻿using AppSerialization.Storage;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization
{
    public class FactoryService
    {
        public IDataStorage<Tasks> GetTaskRepository()
        {
          return new TasksRepository();
        }

        public IDataStorage<Project> GetProjectRepository()
        {
            return new ProjectRepository();
        }

        public IDataStorage<ScheduledTasks> GetScheduledTasksRepository()
        {
           return new ScheduledTasksRepository();
        }

        public IDataStorage<DeletedTasks> GetDeletedTasksRepository()
        {
            return new DeletedTasksRepository();
        }

        public IDataStorage<UserTasks> GetUserTaskRepository()
        {
            return  new UserTaskRepository();
        }

        public IDataStorage<User> GetUserRepository()
        {
            return  new UserRepository();
        }

        public IDataStorage<Token> GetTokenRepository()
        {
            return new TokenRepository();
        }

    }
}
