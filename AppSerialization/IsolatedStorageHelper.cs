﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDatabaseLogic
{
    /// <summary>
    /// Не доделано, не использовать
    /// </summary>
    class IsolatedStorageHelper
    {
        private string _filePath;
        private IsolatedStorageFile isoStore;
        public IsolatedStorageHelper(string filePath)
        {
            _filePath = filePath;
            isoStore= IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);
        }
        public bool Write(string content)
        {
            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(_filePath, FileMode.Open, isoStore))
            {
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.WriteLine(content);
                    return true;
                }
            }
        }

        public string Read()
        {
            if (isoStore.FileExists(_filePath))
            {
                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(_filePath, FileMode.Open, isoStore))
                {
                    using (StreamReader reader = new StreamReader(isoStream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            return String.Empty;
        }
    }
}
