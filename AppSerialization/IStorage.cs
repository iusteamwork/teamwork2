﻿using AppSerialization.Storage;

namespace AppSerialization
{
    public interface IStorage
    {
        TasksRepository TaskDataStorage();
        ProjectRepository ProjectDataStorage();
        ScheduledTasksRepository ScheduledTasksDataStorage();
        DeletedTasksRepository DeletedTasksDataStorage();
        UserTaskRepository UserTaskDataStorage();
        UserRepository UserDataStorage();
    }
}
