﻿using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization
{
    public enum Fields { Status, Name, EvaluationTime, Project, Manager, DeadLine };
    class Search<T> where T: IDataStorage<T>
    {
        private readonly IStorage _storage;
        public Search(IStorage storage)
        {
            _storage = storage;
        }

        public List<Tasks> SearchTask(Fields choose, string str)
        {
            var tasks = _storage.TaskDataStorage().GetData().ToList();
            if (str == null) return new List<Tasks>();
            switch (choose)
            {
                case Fields.Status: return tasks.Where(t => t.ImplementationStatus.Equals(str)).ToList();
                case Fields.Name: return tasks.Where(t => t.Name.Contains(str)).ToList();
                case Fields.EvaluationTime: return tasks.Where(t => t.EvaluationTime.Equals(str)).ToList();
                case Fields.Project: return tasks.Where(t => t.Project.Equals(str)).ToList();
                case Fields.Manager: return tasks.Where(t => t.Manager.Equals(str)).ToList();
                case Fields.DeadLine: return tasks.Where(t => t.DeadLine.Equals(str)).ToList();
                default: return tasks;
            }
        }
    }
}
