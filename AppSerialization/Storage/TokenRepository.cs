﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    class TokenRepository: IDataStorage<Token>
    {
        private readonly XmlSerialize<Token> _xmlSerializer;
        public TokenRepository()
        {
            const string filePath = "Token.xml";
            _xmlSerializer = new XmlSerialize<Token>(filePath);
        }
        public bool Save(Token item)
        {
            if (item == null) return false;
            var tokenList = _xmlSerializer.Deserialize();
            var taskItem = tokenList.FirstOrDefault(t => t.Id.Equals(item.Id));
            if (taskItem == null) return false;
            if (!tokenList.Remove(taskItem)) return false;
            tokenList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(tokenList);
            return true;
        }

        public bool Delete(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var tokenList = _xmlSerializer.Deserialize();
            var tokenItem = tokenList.FirstOrDefault(t => t.Id.Equals(id));
            if (!tokenList.Remove(tokenItem)) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(tokenList);
            return true;
        }

        public bool Add(Token item)
        {
            if (item == null) return false;
            var tokenList = _xmlSerializer.Deserialize().ToList();
            if (tokenList.Count > 0) tokenList = _xmlSerializer.Deserialize();
            tokenList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(tokenList);
            return true;
        }

        public bool WriteToFile(List<Token> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(itemList);
            return true;
        }

        public IEnumerable<Token> GetData()
        {
            return _xmlSerializer.Deserialize();
        }
    }
}
