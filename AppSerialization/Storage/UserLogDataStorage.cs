﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class UserLogDataStorage : ILog<UserLog>
    {
        private readonly XmlSerialize<UserLog> _xmlSerializer;
        public UserLogDataStorage()
        {
            const string filePath = "UserLog.xml";
            _xmlSerializer = new XmlSerialize<UserLog>(filePath);
        }

        /// <summary>
        /// Добавляет сообщение в XML файл
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool WriteMessage(UserLog item)
        {
            if (item == null) return false;
            var taskList = _xmlSerializer.Deserialize();
            taskList.Add(item);
            _xmlSerializer.Serialize(taskList);
            return true;
        }

        public IEnumerable<UserLog> GetData()
        {
            return _xmlSerializer.Deserialize();
        }

        public IEnumerator<UserLog> GetEnumerator()
        {
            return _xmlSerializer.Deserialize().Cast<UserLog>().GetEnumerator();
        }
    }
}
