﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class UserRepository : IDataStorage<User>
    {
        private readonly XmlSerialize<User> _xmlSerializer;
        public UserRepository()
        {
            const string path = "User.xml";
            _xmlSerializer = new XmlSerialize<User>(path);
        }

        public bool Delete(Guid id)
        {
            if (id == Guid.Empty) return false;
            var users = _xmlSerializer.Deserialize();
            var user = users.Where(u => u.Id == id);
            var enumerable = user as User[] ?? user.ToArray();
            if (!enumerable.Any()) return false;
            users.Remove(enumerable.Single());
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(users);
            return true;
        }


        public bool Add(User item)
        {
            if (item.Id == Guid.Empty) return false;
            var users = _xmlSerializer.Deserialize();
            if (users.Any(u => u.Id == item.Id))
            {
                return false;
            }
            users.Add(item);
            _xmlSerializer.Serialize(users);
            return true;
        }

        public IEnumerable<User> GetData()
        {
            return _xmlSerializer.Deserialize();
        }

        public bool Save(User item)
        {
            if (item == null) return false;
            var  userList = _xmlSerializer.Deserialize();
            var user = userList.FirstOrDefault(t => t.Id.Equals(item.Id));
            if (user == null) return false;
            if (!userList.Remove(user)) return false;
            userList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(userList);
            return true;
        }

        public bool WriteToFile(List<User> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(itemList);
            return true;
        }
    }
}
