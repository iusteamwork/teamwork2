﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class TasksRepository: IDataStorage<Tasks>
    {
        private readonly XmlSerialize<Tasks> _xmlSerializer;
        public TasksRepository()
        {
            const string filePath = "Tasks.xml";
            _xmlSerializer = new XmlSerialize<Tasks>(filePath);
        }

        /// <summary>
        /// Сохраняет измененный обьект
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Save(Tasks item)
        {
            if (item == null) return false;
            var taskList = _xmlSerializer.Deserialize();
            var taskItem = taskList.FirstOrDefault(t => t.Id.Equals(item.Id));
            if (taskItem == null) return false;
            if (!taskList.Remove(taskItem)) return false;
            taskList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return true;
        }

        /// <summary>
        /// Удаляет задачу с XML файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            if (id == Guid.Empty) throw new InvalidOperationException("Empty GUID");
            var taskList = _xmlSerializer.Deserialize();
            var taskItem = taskList.FirstOrDefault(t => t.Id.Equals(id));
            if (!taskList.Remove(taskItem)) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return true;
        }

        public bool Add(Tasks item)
        {
            if (item== null) return false;
            var taskList = _xmlSerializer.Deserialize().ToList();
            if (taskList.Count > 0) taskList = _xmlSerializer.Deserialize();
                //Проверка на существования задачи с таким же Guid или именем
            if (taskList.Any(t => t.Id.Equals(item.Id) || t.Name.Equals(item.Name))) return false;
                taskList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return true;
        }

        /// <summary>
        /// Добавляет задачу в XML файл
        /// </summary>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public bool Add(List<Tasks> itemList)
        {
            if (itemList == null) return false;
            var taskList = _xmlSerializer.Deserialize().ToList();
            if (taskList.Count > 0 ) taskList = _xmlSerializer.Deserialize();
                foreach (var task in itemList)
                {
                    //Проверка на существования задачи с таким же Guid или именем
                    if (taskList.Any(t => t.Id.Equals(task.Id) || t.Name.Equals(task.Name))) return false;
                    taskList.Add(task);
                }
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return true;
        }


        /// <summary>
        /// Загружает все задачи
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tasks> GetData()
        {
            return _xmlSerializer.Deserialize();
        }

        public bool WriteToFile(List<Tasks> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(itemList);
            return true;
        }
    }
}
