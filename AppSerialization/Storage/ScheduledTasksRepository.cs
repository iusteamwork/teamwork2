﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class ScheduledTasksRepository : IDataStorage<ScheduledTasks>
    {
        private readonly XmlSerialize<ScheduledTasks> _xmlSerializer;
        public ScheduledTasksRepository()
        {
            _xmlSerializer = new XmlSerialize<ScheduledTasks>("ScheduledTask.xml");
        }

        public bool Save(ScheduledTasks item)
        {
            if (item == null) return false;
            if (item.Task == Guid.Empty || item.User == Guid.Empty) return false;
            var scheduledTasks = _xmlSerializer.Deserialize();
            var scheduleTaskItem = scheduledTasks.FirstOrDefault(st => st.Task.Equals(item.Task));
            if (scheduleTaskItem == null) return false;
            scheduledTasks.Remove(scheduleTaskItem);
            scheduledTasks.Add(item);
            _xmlSerializer.Serialize(scheduledTasks);
            return true;
        }

        public bool Add(ScheduledTasks item)
        {
            if (item == null) return false;
            if (item.Task == Guid.Empty || item.User == Guid.Empty) return false;
            var scheduledTasks = _xmlSerializer.Deserialize();
            var task = scheduledTasks.Where(st => st.User.Equals(item.User) && st.Task.Equals(item.Task));
            if (task.Any()) return false;
            scheduledTasks.Add(item);
            _xmlSerializer.Serialize(scheduledTasks);
            return true;
        }

        public bool Delete(Guid taskId)
        {
            if (taskId == Guid.Empty) return false;
            var scheduledTasks = _xmlSerializer.Deserialize();
            var tasksItems = scheduledTasks.FirstOrDefault(st => st.Task.Equals(taskId));
            scheduledTasks.Remove(tasksItems);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(scheduledTasks);
            return true;
        }


        public IEnumerable<ScheduledTasks> GetData()
        {
            return _xmlSerializer.Deserialize();
        }

        public bool WriteToFile(List<ScheduledTasks> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(itemList);
            return true;
        }
    }
}
