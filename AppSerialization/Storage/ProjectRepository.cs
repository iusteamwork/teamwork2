﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class ProjectRepository: IDataStorage<Project>
    {
        private readonly XmlSerialize<Project> _xmlSerializerProject;

        public ProjectRepository()
        {
            _xmlSerializerProject = new XmlSerialize<Project>("Project.xml");
        }

        public bool Save(Project item)
        {
            if (item == null) return false;
            if (item.Id == Guid.Empty) return false;
            var projectList = _xmlSerializerProject.Deserialize();
            var projectItem = projectList.FirstOrDefault(t => t.Id.Equals(item.Id));
            if (projectItem == null) return false;
            projectList.Remove(projectItem);
            projectList.Add(item);
            _xmlSerializerProject.ClearXmlFile();
            _xmlSerializerProject.Serialize(projectList);
            return true;
        }

        public bool Delete(Guid id)
        {
            if (id == Guid.Empty) return false;
            var projectList = _xmlSerializerProject.Deserialize();
            var projectItem = projectList.FirstOrDefault(t => t.Id.Equals(id));
            if (projectItem == null) return false;
            if (!projectList.Remove(projectItem)) return false;
            _xmlSerializerProject.ClearXmlFile();
            _xmlSerializerProject.Serialize(projectList);
            return true;
        }

        public bool Add(Project item)
        {
            if (item == null) return false;
            var projectList = _xmlSerializerProject.Deserialize();
            var check = projectList.Where(p => p.Id.Equals(item.Id) || p.Name.Equals(item.Name));
            if (check.Any()) return false;
            projectList.Add(item);
            _xmlSerializerProject.Serialize(projectList);
            return true;
        }

        public IEnumerable<Project> GetData()
        {
            return _xmlSerializerProject.Deserialize();
        }
        public bool WriteToFile(List<Project> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializerProject.ClearXmlFile();
            _xmlSerializerProject.Serialize(itemList);
            return true;
        }
    }
}
