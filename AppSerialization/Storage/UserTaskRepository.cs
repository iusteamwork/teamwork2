﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class UserTaskRepository : IDataStorage<UserTasks>
    {
        private readonly XmlSerialize<UserTasks> _userTaskSerialization;
        public UserTaskRepository()
        {
            const string filePath = "UserTask.xml";
            _userTaskSerialization = new XmlSerialize<UserTasks>(filePath);
        }

        public bool Add(UserTasks userTask)
        {
            if (userTask == null) return false;
            if (userTask.User == Guid.Empty || userTask.Task == Guid.Empty) return false;
            var userTasks = _userTaskSerialization.Deserialize();
            var check = userTasks.Where(ut => ut.User.Equals(userTask.User) && ut.Task.Equals(userTask.Task));
            if (check.Any()) return false;
            userTasks.Add(userTask);
            _userTaskSerialization.Serialize(userTasks);
            return true;
        }

        public bool Delete(Guid taskId)
        {
            if (taskId == Guid.Empty) return false;
            var userTasks = _userTaskSerialization.Deserialize();
            var task = userTasks.FirstOrDefault(ut => ut.Task.Equals(taskId));
            userTasks.Remove(task);
            _userTaskSerialization.ClearXmlFile();
            _userTaskSerialization.Serialize(userTasks);
            return true;
        }


        public bool Save(UserTasks item)
        {
            if (item == null) return false;
            var userTasks = _userTaskSerialization.Deserialize();
            var taskItem = userTasks.FirstOrDefault(ut => ut.Task.Equals(item.Task));
            if (taskItem==null) return false;
            userTasks.Remove(taskItem);
            userTasks.Add(item);
            _userTaskSerialization.ClearXmlFile();
            _userTaskSerialization.Serialize(userTasks);
            return true;
        }

        public IEnumerable<UserTasks> GetData()
        {
            return _userTaskSerialization.Deserialize();
        }


        public bool WriteToFile(List<UserTasks> itemList)
        {
            if (itemList == null) return false;
            _userTaskSerialization.ClearXmlFile();
            _userTaskSerialization.Serialize(itemList);
            return true;
        }
    }
}
