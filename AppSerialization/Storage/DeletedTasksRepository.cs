﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.Interfaces;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class DeletedTasksRepository : IDataStorage<DeletedTasks>
    {
        private readonly XmlSerialize<DeletedTasks> _xmlSerializer;
        public DeletedTasksRepository()
        {
            _xmlSerializer = new XmlSerialize<DeletedTasks>("DeletedTask.xml");
        }


        /// <summary>
        /// Добавляет удаленную задачу в XML файл
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool Add(DeletedTasks task)
        {
            if (task == null) return true;
            if (task.Id == Guid.Empty || task.Project == Guid.Empty || task.Manager == Guid.Empty) return true;
            var tasks = _xmlSerializer.Deserialize();
            tasks.Add(task);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(tasks);
            return true;
        }


        public bool Save(DeletedTasks item)
        {
            if (item == null) return false;
            var taskList = _xmlSerializer.Deserialize();
            var taskItem = taskList.FirstOrDefault(t => t.Id.Equals(item.Id));
            if (taskItem == null) return false;
            if (!taskList.Remove(taskItem)) return false;
            taskList.Add(item);
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return false;
        }


        public bool Delete(Guid id)
        {
            if (id == Guid.Empty) return false;
            var taskList = _xmlSerializer.Deserialize();
            var task = taskList.FirstOrDefault(t => t.Id.Equals(id));
            if (task == null) return false;
            if (!taskList.Remove(task)) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(taskList);
            return true;
        }

        public IEnumerable<DeletedTasks> GetData()
        {
            return _xmlSerializer.Deserialize();
        }

        public bool WriteToFile(List<DeletedTasks> itemList)
        {
            if (itemList == null) return false;
            _xmlSerializer.ClearXmlFile();
            _xmlSerializer.Serialize(itemList);
            return true;
        }

    }
}
