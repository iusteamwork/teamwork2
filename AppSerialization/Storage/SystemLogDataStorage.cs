﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.SerializationTypes;

namespace AppSerialization.Storage
{
    public class SystemLogDataStorage : ILog<SystemLog>
    {
        private readonly string filePath;
        private XmlSerialize<SystemLog> xmlSerializer;
        public SystemLogDataStorage()
        {
            filePath = "SystemLog.xml";
            xmlSerializer = new XmlSerialize<SystemLog>(filePath);
        }
        /// <summary>
        /// Добавляет сообщение в XML файл
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool WriteMessage(SystemLog item)
        {
            if (item != null)
            {
                var logList = xmlSerializer.Deserialize();
                logList.Add(item);
                xmlSerializer.Serialize(logList);
                return true;
            }
            return false;
        }
    }
}
