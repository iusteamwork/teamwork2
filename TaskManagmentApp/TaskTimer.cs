﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using AppSerialization;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp
{
    class TaskTimer
    {
        private Timer _timer;
        private RepresentTask _tempRepresentTask;
       // private Label _LabelTimer;
        private DateTime _time;
        private TaskViewModel _owner;


        /*public TaskViewModel Owner
        {
            get;
            set;

        }*/

        private TaskTimer()
        {
        }

        public TaskTimer(TaskViewModel Owner)
        {
            _owner = Owner;
        }


        public bool StartTimer(RepresentTask TempRepresentTask)
        {
            if (TempRepresentTask == null)
            {
                return false;
            }

            _tempRepresentTask = TempRepresentTask;
            //_LabelTimer = Timer;
            _time = _tempRepresentTask.ElapsedTime;
            _owner.StrTimer = _time.ToString("T");
            _timer = new Timer();
            _timer.AutoReset = true;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Interval = 990;
            _timer.Start();
            return true;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _time = _time.AddSeconds(1);
            _owner.StrTimer = _time.ToString("T");
        }


        public bool StopTimer()
        {
            if(_timer==null)
            {
                return false;
            }
            _tempRepresentTask.ElapsedTime = _time;
            _timer.Stop();
            _timer = null;
            var taskStorege = new FactoryService();
            _owner.StrTimer = "";
            var temp = new Tasks()
            {
                About = _tempRepresentTask.About,
                DeadLine = _tempRepresentTask.DeadLine,
                ElapsedTime = _tempRepresentTask.ElapsedTime,
                EvaluationTime = _tempRepresentTask.EvaluationTime,
                Id = _tempRepresentTask.Id,
                ImplementationStatus = _tempRepresentTask.ImplementationStatus,
                Manager = _tempRepresentTask.Manager,
                Name = _tempRepresentTask.Name,
                PlannedOut = _tempRepresentTask.PlannedOut,
                Project = _tempRepresentTask.Project
            };
            return taskStorege.GetTaskRepository().Save(temp);// TaskDataStorage().Save(temp);
        }
    }
}
