﻿using AppSerialization;
using AppSerialization.Service;
using AppSerialization.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.ViewModels
{
    class UserLogViewModel
    {
        UserLogDataStorage _logs = new UserLogDataStorage();

        public void SerializeLog(Guid Id, string action, string task, string image)
        {
            UserLog log = new UserLog();
            log.UserId = Id;
            var users = new UserService(new FactoryService());
            log.User = users.GetUser(log.UserId).Login;
            log.Action = action;
            log.Image = image;
            log.Task = task;
            log.Date = DateTime.Now;
            var logSerialize = new UserLogDataStorage().WriteMessage(log);
        }

      public List<UserLog>  ShowLogs(Guid curentUser)
        {
            return _logs.GetData().Where(c => c.UserId == curentUser).Select(c=>c).ToList();   
        }

    }
}
