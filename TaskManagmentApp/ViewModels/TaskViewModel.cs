﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using AppSerialization.Service;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using AppSerialization;
using System.Windows.Controls;
using AppSerialization.Storage;

namespace TaskManagmentApp.ViewModels
{
    class TaskViewModel : BaseViewModel
    {
        #region Properties
        private List<RepresentTask> _taskList;
        private List<RepresentTask> _tempTaskList;
        private List<string> _projectList;

        private List<RepresentScheduledTask> _scheduledTaskList;
        private List<RepresentScheduledTask> _tempScheduledTaskList;

        private List<RepresentDeleteTask> _deletedTaskList;
        private List<RepresentDeleteTask> _tempDeleteTask;

        private TaskTimer _timer;
        private string _strTimer;
        private string _btnTimer;

        private string _setProdjectTasks;
        private string _textSearchTasks;
        private bool _completeCheckedTasks;
        private DateTime? _setTasksDateTime;

        private string _setProdjectScheduledTasks;
        private string _textSearchScheduledTasks;
        private DateTime? _setScheduleTasksDateTime;

        private string _setProdjectDeletedTasks;
        private string _textSearchDeletedTasks;
        private string _userName;


        private Guid _userGuid;
        private string _image;
        private string _user;
        private string _task;
        private DateTime _date;
        private string _action;
        private Visibility _taskButtonsVisibility;


        // controls visibility
        public Visibility TaskButtonsVisibility
        {
            get { return _taskButtonsVisibility; }
            set { _taskButtonsVisibility = value; OnPropertyChanged(); }
        }
        public Guid UserGuid
        {
            get { return _userGuid; }
            set { _userGuid = value; OnPropertyChanged(); }
        }

        public string Image
        {
            get { return _image; }
            set { _image = value; OnPropertyChanged(); }
        }

        public string User
        {
            get { return _user; }
            set { _user = value; OnPropertyChanged(); }
        }

        public string Task
        {
            get { return _task; }
            set { _task = value; OnPropertyChanged(); }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; OnPropertyChanged(); }
        }
        public string Action
        {
            get { return _action; }
            set { _action = value; OnPropertyChanged(); }
        }

        public string CurrentUser
        {
            get { return _userName; }
            set { _userName = "Пользователь:  " + value; OnPropertyChanged(); }
        }
        public string TextSearchDeletedTasks
        {
            get { return _textSearchDeletedTasks; }
            set { _textSearchDeletedTasks = value; OnPropertyChanged(); }
        }

        public string SetProjectDeletedTasks
        {
            get { return _setProdjectDeletedTasks; }
            set { _setProdjectDeletedTasks = value; OnPropertyChanged(); }
        }
        
        public bool CompleteCheckedScheduledTasks
        {
            get { return _completeCheckedTasks; }
            set { _completeCheckedTasks = value; OnPropertyChanged(); }
        }


        public string TextSearchScheduledTasks
        {
            get { return _textSearchScheduledTasks; }
            set { _textSearchScheduledTasks = value; OnPropertyChanged(); }
        }

        public string SetProjectScheduledTasks
        {
            get { return _setProdjectScheduledTasks; }
            set { _setProdjectScheduledTasks = value; OnPropertyChanged(); }
        }

        public DateTime? SetTasksDateTime
        {
            get { return _setTasksDateTime; }
            set{ _setTasksDateTime = value; OnPropertyChanged();}
        }

        public DateTime? SetScheduleTasksDateTime
        {
            get { return _setScheduleTasksDateTime; }
            set { _setScheduleTasksDateTime = value; OnPropertyChanged(); }
        }
        public bool CompleteChecked
        {
            get { return _completeCheckedTasks; }
            set { _completeCheckedTasks = value; OnPropertyChanged(); }
        }


        public string TextSearch
        {
            get { return _textSearchTasks; }
            set { _textSearchTasks = value; OnPropertyChanged(); }
        }

        public string SetProjectTasks
        {
            get { return _setProdjectTasks; }
            set{ _setProdjectTasks = value; OnPropertyChanged();}
        }

        public string StrTimer
        {
            get { return _strTimer; }
            set {_strTimer = value; OnPropertyChanged();}
        }

        public string BtnTimer
        {
            get { return _btnTimer; }
            set { _btnTimer = value; OnPropertyChanged(); }
        }

        public List<RepresentTask> TaskList
        {
            get { return _taskList; }
            set { _taskList = value; OnPropertyChanged(); }
        }


        public List<RepresentTask> TempTaskList
        {
            get { return _tempTaskList; }
            set { _tempTaskList = value; OnPropertyChanged(); }
        }

        public List<string> ProjectList
        {
            get { return _projectList; }
            set { _projectList = value; OnPropertyChanged(); }
        }

        public List<RepresentScheduledTask> ScheduledTaskList
        {
            get { return _scheduledTaskList; }
            set { _scheduledTaskList = value; OnPropertyChanged(); }
        }


        public List<RepresentScheduledTask> TempScheduledTaskList
        {
            get { return _tempScheduledTaskList; }
            set { _tempScheduledTaskList = value; OnPropertyChanged(); }
        }

        public List<RepresentDeleteTask> DeletedTaskList
        {
            get { return _deletedTaskList; }
            set { _deletedTaskList = value; OnPropertyChanged(); }
        }

        public List<RepresentDeleteTask> TempDeletedTaskList
        {
            get { return _tempDeleteTask; }
            set { _tempDeleteTask = value; OnPropertyChanged(); }
        }
        #endregion
        //Constructor
        public TaskViewModel()
        {
            BtnTimer = "Старт";
            TaskButtonsVisibility = Visibility.Hidden;
        }

        /// <summary>
        /// Conclusion projects in ComboBox
        /// </summary>
        public void OutputProjectInComboBox(Guid userId)
        {
            var taskService = new TaskService(new FactoryService());
            var task = taskService.GetUserTasks(userId).GroupBy(i => i.Project);
            var projectService = new ProjectService(new FactoryService());
            ProjectList = projectService.GetTextualProjectList(task);
        }

        /// <summary>
        /// Show login for current user
        /// </summary>
        /// <param name="userId"></param>
        public void ShowUserName(Guid userId)
        {
            if (userId != Guid.Empty)
            {
                var userService = new UserService(new FactoryService());
                CurrentUser = userService.GetUser(userId).Login;
            }
        }

        /// <summary>
        /// Conclusion project scheduled tasks for user in DataGrid
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="userId"></param>
        internal void OutProjectScheduledTasks(string projectName, Guid userId)
        {
            var projectDataStorage = new ProjectService(new FactoryService());
            var scheduledTaskService = new ScheduledTaskService(new FactoryService());
            if (projectName != null && projectName.Equals("Все проекты"))
            {
                var tasks = scheduledTaskService.GetTasksByUser(userId);
                ScheduledTaskList = null;
                if (tasks.Any())
                {
                    ScheduledTaskList = tasks;
                    TempScheduledTaskList = tasks;
                }

            }
            else
            {
                var projectId = projectDataStorage.GetProjectIdByName(projectName);
                if (projectId == Guid.Empty) return;
                var tasks = scheduledTaskService.GetTasks(userId, projectId);
                ScheduledTaskList = null;
                if (tasks.Any())
                {
                    ScheduledTaskList = tasks;
                    TempScheduledTaskList = tasks;
                }
            }
        }


        /// <summary>
        /// Conclusion user tasks in DataGrid
        /// </summary>
        /// <param name="item"></param>
        /// <param name="userId"></param>
        internal void OutputTaskInDataGrid(Guid userId)
        {
            _userGuid = userId;
            var taskService = new TaskService(new FactoryService());
            var task = taskService.GetUserTasks(userId);
            TaskList = task;
            TempTaskList = task;
        }
        /// <summary>
        /// Conclusion user deleted tasks in DataGrid
        /// </summary>
        /// <param name="userId"></param>
        internal void OutputDeletedTaskInDataGrid(Guid userId)
        {
            _userGuid = userId;
            var deletedTaskService = new DeletedTaskService(new FactoryService());
            var task = deletedTaskService.GetTasks(userId);
            DeletedTaskList = task;
            TempDeletedTaskList = task;
        }
        /// <summary>
        /// Conclusion user scheduled tasks in DataGrid
        /// </summary>
        /// <param name="userId"></param>
        internal void OutputScheduledTaskInDataGrid(Guid userId)
        {
            _userGuid = userId;
            var scheduledTaskService = new ScheduledTaskService(new FactoryService());
            var task = scheduledTaskService.GetTasksByUser(userId);
            ScheduledTaskList = task;
            TempScheduledTaskList = task;
        }

        /// <summary>
        /// Checking scheduled task
        /// </summary>
        /// <param name="task"></param>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public bool CheckScheduledTask(Guid task, Guid userGuid)
        {
            if (userGuid == Guid.Empty || task == Guid.Empty)
            {
                throw new Exception("Empty Guid for task or user");
            }
            var scheduledTasksStorage = new FactoryService();
            var tasks = scheduledTasksStorage.GetScheduledTasksRepository().GetData();
            return tasks.Any(i => i.Task.Equals(task) && i.User.Equals((userGuid)));
        }

        /// <summary>
        /// Completion of the task
        /// </summary>
        /// <param name="representTask"></param>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public bool CompletedTask(RepresentScheduledTask representTask, Guid userGuid)
        {
            if (representTask == null) return false;
            var scheduledTaskService = new ScheduledTaskService(new FactoryService());
            var deletedRepository = new DeletedTasksRepository();
            //Удаление с распределенных задач
            if (scheduledTaskService.DeleteScheduleTask(new ScheduledTasks{Task = representTask.Task, User = userGuid}))
            {
                var taskService = new TaskService(new FactoryService());
                if (taskService.CompleteTask(representTask.Task))
                {
                    //taskService.CompleteTask(new RepresentTask
                    //{
                    //    Id = representTask.Task,
                    //    About = representTask.About,
                    //    ElapsedTime = representTask.ElapsedTime,
                    //    DateEnd = representTask.DateEnd,
                    //    DateStart = representTask.DateStart,
                    //    EvaluationTime = representTask.EvaluationTime,
                    //    PlannedOut = representTask.PlannedoOut,
                    //    DeadLine = representTask.DeadLine,
                    //    ImplementationStatus = representTask.ImplementationStatus,
                    //    Manager = representTask.Manager,
                    //    Project = representTask.Project,
                    //    Name = representTask.Name,
                    //    ManagerName = representTask.ManagerLogin,
                    //    ProjectName = representTask.ProjectName,
                    //    ImplementationIconSource = representTask.ImplementationIconSource,
                    //    CreateDate = representTask.CreateDateTime
                    //});

                    //Добавление в репозиторий удаленных задач
                    if (deletedRepository.Add(new DeletedTasks
                    {
                        Id = representTask.Task,
                        About = representTask.About,
                        Name = representTask.Name,
                        DeadLine = representTask.DeadLine,
                        ElapsedTime = representTask.ElapsedTime,
                        EvaluationTime = representTask.EvaluationTime,
                        ImplementationStatus = 1,
                        Manager = representTask.Manager,
                        PlannedOut = representTask.PlannedoOut,
                        Project = representTask.Project,
                        User = userGuid,
                        DateEnd = representTask.DateEnd,
                        DateStart = representTask.DateStart,
                    }))
                    {
                        //Обновление грида
                        var taskList = TaskList;
                        var task = taskList.FirstOrDefault(t => t.Id.Equals(representTask.Task));
                        if (task != null) task.ImplementationIconSource = "Resources/complete.png";
                        TaskList = null;
                        TaskList = taskList;
                        OutputDeletedTaskInDataGrid(representTask.User);
                        OutputScheduledTaskInDataGrid(representTask.User);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Delete task
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeletedTask(RepresentDeleteTask obj)
        {
            var taskForDeleted = new DeletedTasksRepository();
            var tasks = new DeletedTaskService(new FactoryService());
            var factoryService = new FactoryService();
            var deletedTaskData = factoryService.GetDeletedTasksRepository().GetData();
            var item = deletedTaskData.First(c => c.Id == obj.Id);
            tasks.DeletePermanently(item.Id);
            return taskForDeleted.Delete(obj.Id);
        }
        /// <summary>
        /// Delete all tasks
        /// </summary>
        /// <param name="obj"></param>
        public void DeletedAll(RepresentDeleteTask obj)
        {
            var tasks = new DeletedTaskService(new FactoryService());
            var factoryService = new FactoryService();
            var deletedTaskData = factoryService.GetDeletedTasksRepository().GetData();
            var task = deletedTaskData.Select(c => c).ToList();
            foreach (var item in task)
            {
                tasks.DeletePermanently(item.Id);
            }
        }
        /// <summary>
        /// Restore task
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RestoreTask(RepresentDeleteTask obj)
        {
            var scheduledTasksRepository = new ScheduledTasksRepository();
            var deletedTaskService = new DeletedTaskService(new FactoryService());
            var taskService = new TaskService(new FactoryService());
            var factoryService = new FactoryService();
            var taskData = factoryService.GetDeletedTasksRepository().GetData();
            var item = taskData.First(c => c.Id == obj.Id);
            var schelItem = new ScheduledTasks{ DateEnd = item.DateEnd, DateStart = item.DateStart, Task = item.Id, User = item.User };
            taskService.CompleteTask(obj.Id);
            return deletedTaskService.Restore(obj.Id) && scheduledTasksRepository.Add(schelItem) && deletedTaskService.DeletePermanently(item.Id);
        }
        
        public bool Timer(Label timer, RepresentTask tempRepresentTask)
        {
            //EvaluationTime = _tempRepresentTask.EvaluationTime;
            if (_timer != null)
            {
                _timer.StopTimer();
                _timer = null; 
                BtnTimer = "Старт";
                return true;
               
            }

            _strTimer = "Timer";
            _timer = new TaskTimer(this);
            BtnTimer = "Стоп";
            return _timer.StartTimer(tempRepresentTask);
        }

        public void Init()
        {
            BtnTimer = "Старт";
            SetTasksDateTime = null;
            SetScheduleTasksDateTime = null;
            FilterDeletedTasks();
            FilterScheduledTasks();
            FilterTasks();
            /*FilterTasks();
            FilterScheduledTasks();
            FilterDeletedTasks();*/
        }

        public void ShowLog(UserLog log)
        {
            if (log == null) return;
            Image = log.Image;
            User = log.User;
            Task = log.Task;
            Date = log.Date;
            Action = log.Action;
        }


        /// <summary>
        /// Filter Tasks
        /// {
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// 

        public void WatermarkFilterTasks(string textSearch)
        {
            _textSearchTasks = textSearch;
            FilterTasks();
        }


        public void FilterTasks()
        {
            TaskList = TempTaskList;
            if (!string.IsNullOrWhiteSpace(_setProdjectTasks) && _setProdjectTasks!="Все проекты")
                TaskList = TaskList.Where(p => p.ProjectName.ToLower().Contains(_setProdjectTasks.ToLower())).ToList();
            if (!string.IsNullOrWhiteSpace(_textSearchTasks))
                TaskList = TaskList.Where(p => p.Name.ToLower().Contains(_textSearchTasks.ToLower())).ToList();
            if(_setTasksDateTime != null&&TaskList!=null)
                TaskList = TaskList.Where(p => p.DeadLine.Date == _setTasksDateTime.Value.Date).ToList();
            //TaskList = TaskList.Where(p => p.ImplementationStatus == Convert.ToByte(_completeCheckedTasks)).ToList();
             
        }


        public void WatermarkFilterScheduledTasks(string textSearch)
        {
            _textSearchScheduledTasks = textSearch;
            FilterScheduledTasks();
        }


        public void FilterScheduledTasks()
        {
            ScheduledTaskList = _tempScheduledTaskList;
            if (!string.IsNullOrWhiteSpace(_setProdjectScheduledTasks)&&ScheduledTaskList!=null)
                ScheduledTaskList = ScheduledTaskList.Where(p => p.ProjectName.ToLower().Contains(_setProdjectScheduledTasks.ToLower())).ToList();
            if (!string.IsNullOrWhiteSpace(_textSearchScheduledTasks)&&ScheduledTaskList!=null)
                ScheduledTaskList = ScheduledTaskList.Where(p => p.Name.ToLower().Contains(_textSearchScheduledTasks.ToLower())).ToList();
            if (_setScheduleTasksDateTime != null && ScheduledTaskList != null)
                ScheduledTaskList = ScheduledTaskList.Where(c => c.DateStart.Date <= _setScheduleTasksDateTime.Value.Date && c.DateEnd.Date >= _setScheduleTasksDateTime.Value.Date).ToList();
            if(ScheduledTaskList!=null)
                ScheduledTaskList = _scheduledTaskList.Where(p => p.ImplementationStatus == Convert.ToByte(_completeCheckedTasks)).ToList();
             
        }

        public void WatermarkFilterDeletedTasks(string textSearch)
        {
            _textSearchDeletedTasks = textSearch;
            FilterDeletedTasks();
        }


        public void FilterDeletedTasks()
        {
            DeletedTaskList = _tempDeleteTask;
            if (!string.IsNullOrWhiteSpace(_setProdjectDeletedTasks))
                DeletedTaskList = DeletedTaskList.Where(p => p.ProjectName.ToLower().Contains(_setProdjectDeletedTasks.ToLower())).ToList();
            if (!string.IsNullOrWhiteSpace(_textSearchDeletedTasks))
                DeletedTaskList = DeletedTaskList.Where(p => p.Name.ToLower().Contains(_textSearchDeletedTasks.ToLower())).ToList();
        }

        /// <summary>
        /// Set selected task, status Completed
        /// </summary>
        /// <param name="representTask"></param>
        internal void CompleteTask(RepresentTask representTask)
        {
            if (representTask == null) return;
            var taskService = new TaskService(new FactoryService());
            if (taskService.CompleteTask(representTask.Id))
            {
                var taskList = TaskList;
                var task= taskList.FirstOrDefault(t => t.Id.Equals(representTask.Id));
                if (task != null) task.ImplementationIconSource = "Resources/complete.png";
                TaskList = null;
                TaskList = taskList;
            }
        }

        public void OutProjectTasksForUser(string projectName, Guid userGuid)
        {
            var projectDataStorage = new ProjectService(new FactoryService());
            if (projectName != null && projectName.Equals("Все проекты"))
            {
                OutputTaskInDataGrid(userGuid);
            }
            else
            {
                var projectId = projectDataStorage.GetProjectIdByName(projectName);
                if (projectId == Guid.Empty) return;
                var taskService = new TaskService(new FactoryService());
                var tasks = taskService.GetTasks(projectId, userGuid);
                TaskList = null;
                if (tasks.Any()) TaskList = tasks;
                TempTaskList = tasks;
            }
        }

        public void ShowControlsForSelectedTask(RepresentTask representTask, Guid userGuid)
        {
            if (representTask != null)
            {
                var scheduledTaskService = new ScheduledTaskService(new FactoryService());
                if (!scheduledTaskService.GetUserScheduledTasks(userGuid).Any(t => t.Task.Equals(representTask.Id)) &&
                    representTask.ImplementationStatus != 1)
                    TaskButtonsVisibility = Visibility.Visible;
                else TaskButtonsVisibility = Visibility.Hidden;
            }
        }

        #region Orderds 
        public void OrderTaskList(string option)
        {
            switch (option)
            {
                case "byStatus": TaskList = TaskList.OrderByDescending(t=>t.ImplementationStatus).ToList(); break;
                case "byTask": TaskList = TaskList.OrderByDescending(t => t.Name).ToList(); break;
                case "byDate": TaskList = TaskList.OrderByDescending(t=>t.CreateDate).ToList(); break;
                case "byEvaluation": TaskList = TaskList.OrderByDescending(t => t.EvaluationTime).ToList(); break;
            }
        }

        public void OutScheduledCompleteTasks(string option)
        {
            if (ScheduledTaskList != null)
            {
                switch (option)
                {
                    case "byAll":
                        ScheduledTaskList= TempScheduledTaskList;
                        break;
                    case "byComplete":
                        ScheduledTaskList = TempScheduledTaskList.Where(p => p.ImplementationStatus == 1).ToList();
                        break;
                    case "byUncomplete":
                        ScheduledTaskList = TempScheduledTaskList.Where(p => p.ImplementationStatus == 0).ToList();
                        break;
                }
            }
        }

        public void OrderScheduledList(string option)
        {
            switch (option)
            {
                case "byStatus": ScheduledTaskList = TempScheduledTaskList.OrderByDescending(t => t.ImplementationStatus).ToList(); break;
                case "byTask": ScheduledTaskList = TempScheduledTaskList.OrderByDescending(t => t.Name).ToList(); break;
                case "byDate": ScheduledTaskList = TempScheduledTaskList.OrderByDescending(t => t.CreateDateTime).ToList(); break;
                case "byEvaluation": ScheduledTaskList = TempScheduledTaskList.OrderByDescending(t => t.EvaluationTime).ToList(); break;
            }
        }

        /// <summary>
        /// Method for processing out completed and outstanding tasks
        /// </summary>
        public void OutCompleteTasks(string option)
        {
            switch (option)
            {
                case "byAll":
                    TaskList = TempTaskList; break;
                case "byComplete": TaskList = TempTaskList.Where(p => p.ImplementationStatus == 1).ToList(); break;
                case "byUncomplete": TaskList = TempTaskList.Where(p => p.ImplementationStatus == 0).ToList(); break;
            }

        }

    #endregion


    }
}
