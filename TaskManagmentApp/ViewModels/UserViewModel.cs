﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using AppSerialization;
using AppSerialization.Service;
using AppSerialization.Storage;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.ViewModels
{
    class UserViewModel: BaseViewModel
    {
        private string _login;
        private string _password;
        private bool _adminRole;
        private bool _userRole;
        private string _message;

        public string Login
        {
            get { return _login; }
            set { _login = value; OnPropertyChanged();}
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(); }
        }
        public bool AdminRole
        {
            get { return _adminRole; }
            set { _adminRole = value; OnPropertyChanged(); }
        }
        public bool UserRole
        {
            get { return _userRole; }
            set { _userRole = value; OnPropertyChanged();}
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged();}
        }

        /// <summary>
        /// Add new user to db
        /// </summary>
        /// <returns></returns>
        public bool AddUser(AdminViewModel adminViewModel)
        {
            if (string.IsNullOrEmpty(Login) || string.IsNullOrEmpty(Password))
            {
                Message = "Логин или пароль не заполнены";
                return false;
            }
            if (Regex.Match(Login, "[а-яА-Яa-zA-z]").Success)
            {               
                var userService = new UserService(new FactoryService());
                var user=userService.GetUserByLogin(Login);
                //Check user exist
                if (user != null) return false;
                    var role = AdminRole ? 1 : 0;
                    var responseUser = userService.RegistrateUser(Login, Password, (byte) role);
                    if (responseUser!=null)
                    {
                        var users = adminViewModel.UserList;
                        users.Add(new RepresentUser
                        {
                            Id =responseUser.Id,
                            Login = responseUser.Login,
                            RegistrationDateTime = responseUser.RegistrationDateTime,
                            Role = responseUser.Role == 0 ? "Пользователь" : "Администратор"
                        });
                        adminViewModel.UserList = null;
                        adminViewModel.UserList = users;
                        return true;
                    
                }
            }
            Message = "Логин имеет неверный формат";
            return false;
        }
    }
}
