﻿using System;
using AppSerialization;
using AppSerialization.Service;
using AppSerialization.Storage;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.ViewModels
{
    class AddEditTaskViewModel:BaseViewModel
    {
        private string _task;
        private string _about;
        private string _message;
        private DateTime _evaluation;
        private DateTime _deadLine;

        public AddEditTaskViewModel()
        {
            DeadLine = DateTime.Now;
        }
        public string Task
        {
            get { return _task; }
            set { _task = value; OnPropertyChanged(); }
        }

        public string About
        {
            get { return _about; }
            set { _about = value;OnPropertyChanged(); }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged();}
        }

        public DateTime Evaluation
        {
            get { return _evaluation; }
            set { _evaluation = value; OnPropertyChanged();}
        }

        public DateTime DeadLine
        {
            get { return _deadLine; }
            set { _deadLine = value; OnPropertyChanged();}
        }

        /// <summary>
        /// Add task for add window
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="managerGuid"></param>
        /// <returns></returns>
        public bool AddTask(string projectName, Guid managerGuid)
        {
            if (string.IsNullOrEmpty(projectName)) return false;
            if (string.IsNullOrEmpty(Task)) { 
                Message = "Введите имя задачи";
                return false;
            }
            var projectService = new ProjectService(new FactoryService());
            var project = projectName.Split('-')[1];
            var projectId = projectService.GetProjectIdByName(project);
            if (projectId == Guid.Empty) return false;
            var taskService = new TasksRepository();
            return taskService.Add(new Tasks
            {
                Id = Guid.NewGuid(),
                About = About,
                Project = projectId,
                DeadLine = DeadLine,
                EvaluationTime = Evaluation,
                ImplementationStatus = 0,
                Manager = managerGuid,
                Name = Task,
                PlannedOut = 0,
                ElapsedTime = DateTime.MinValue,
                TimeDeleteTask = DateTime.MinValue,
                CreateDate = DateTime.Now
            });
        }

        /// <summary>
        /// Edit task for edit window
        /// </summary>
        /// <param name="task"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool EditTask(RepresentTask task, Guid id)
        {
            if (id == Guid.Empty) return false;
            if (String.IsNullOrEmpty(Task))
            {
                Message = "Введите имя задачи";
                return false;
            }
            var taskStorage = new TasksRepository();
            return taskStorage.Save(new Tasks
            {
                Id = task.Id, 
                About = About, 
                DeadLine = DeadLine,
                EvaluationTime = Evaluation,
                Name = Task,
                Manager = id,
                Project = task.Project,
                ImplementationStatus =  task.ImplementationStatus,
                PlannedOut =  task.ImplementationStatus,
                ElapsedTime = task.ElapsedTime,
                TimeDeleteTask = task.TimeDeleteTask,
                CreateDate = task.CreateDate
            });
        }
    }
}
