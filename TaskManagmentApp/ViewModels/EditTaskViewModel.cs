﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagmentApp.Data.RepersentTypes;

namespace TaskManagmentApp.ViewModels
{
    class EditTaskViewModel: BaseViewModel
    {
        private Guid _id;
        private string _taskName;
        private DateTime _deadLine;
        private string _about;

        public Guid Project
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }
        public string Task
        {
            get { return _taskName; }
            set { _taskName = value; OnPropertyChanged(); }
        }
        public DateTime DeadLine
        {
            get { return _deadLine; }
            set { _deadLine = value; OnPropertyChanged(); }
        }
         public string About
        {
            get { return _about; }
            set { _about = value; OnPropertyChanged(); }
        }



    }
}
