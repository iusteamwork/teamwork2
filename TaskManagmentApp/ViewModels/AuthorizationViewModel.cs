﻿using System;
using System.Security.Cryptography;
using System.Text;
using AppSerialization;
using AppSerialization.Service;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.ViewModels
{
	public class AuthorizationViewModel: BaseViewModel
	{
		private string _login;
		private string _password;
		private string _message;
        public bool _rememberUser;


        public bool RememberUser 
        {
            get { return _rememberUser; }
            set { _rememberUser = value; OnPropertyChanged(); }
        }

		public string Message
		{
			get { return _message; }
			set { _message = value; OnPropertyChanged();}
		}

		public string Login
		{
			get { return _login; }
			set { _login = value; OnPropertyChanged();}
		}

		public string Password
		{
			get { return _password; }
			set { _password = value; OnPropertyChanged();}
		}


		public Result AuthorizeUser()
		{
            Login = "Anton";
            Password = "anton1212";
		    if (!String.IsNullOrEmpty(Login) && !String.IsNullOrEmpty(Password))
		    {
		        var userService = new UserService(new FactoryService());
		        var result=userService.AuthorizeUser(Login, Password);
                if (result != null)
                {
                    if (RememberUser)
                    {
                        var tokenService = new TokenService(new FactoryService());
                        tokenService.AddToken(userService.GetUserIdByLogin(Login));
                    }
                    return result;
                }
                Message = "Неверный логин или пароль";
		        return null;
		    }
		    Message = "Заполните все поля";
		    return null;
		}

        public bool AuthorizeRememberUser()
        {

            return false;
        }
	}


}
