﻿using System;
using AppSerialization;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using System.Collections.Generic;
using AppSerialization.Service;

namespace TaskManagmentApp.ViewModels
{
    public class ScheduleTaskViewModel : BaseViewModel
    {
        private string _project;
        private string _task;
        private string _about;
        private string _deadLine;
        private string _manager;
        private DateTime _startTime;
        private string _endTime;

        //... 
        private readonly Guid _userGuid;
        private Guid _taskGuid;
        private string _evaluationTime;

        public ScheduleTaskViewModel(Guid userGuid)
        {
            _userGuid = userGuid;
        }

        public string EvaluationHours
        {
            get { return _evaluationTime; }
            set { _evaluationTime = value; OnPropertyChanged(); }
        }

        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; OnPropertyChanged(); }
        }

        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; OnPropertyChanged(); }
        }

        public string Task
        {
            get { return _task; }
            set { _task = value; OnPropertyChanged(); }
        }
        public string About
        {
            get { return _about; }
            set { _about = value; OnPropertyChanged(); }
        }
        public string DeadLineDate
        {
            get { return _deadLine; }
            set { _deadLine = value; OnPropertyChanged(); }
        }
        public string Manager
        {
            get { return _manager; }
            set { _manager = value; OnPropertyChanged(); }
        }
        public string Project
        {
            get { return _project; }
            set { _project = value; OnPropertyChanged(); }
        }
        public ScheduleTaskViewModel()
        {
        }

        /// <summary>
        /// /Method initializes the initial values
        /// </summary>
        /// <param name="task"></param>
        public void ShowInfo(RepresentTask task)
        {
            if (task == null) return;
            Project = task.ProjectName;
            Task = task.Name;
            About = task.About;
            Manager = task.ManagerName;
            DeadLineDate = task.DeadLineDate;
            _taskGuid = task.Id;
            EvaluationHours = task.EvaluationHours;
            StartTime = DateTime.Now.Date;
            EndTime = EvaluationHours;
        }

        /// <summary>
        /// ScheduledTask for current user
        /// </summary>
        /// <returns></returns>
        public bool ScheduleTheTask()
        {
            //Check to ensure that the date was chosen
            //if (StartTime <= DateTime.MinValue || EndTime <= DateTime.MinValue) return false;
            //if (StartTime >= EndTime) return false;
            var storage = new FactoryService();
            var scheduledStorage = storage.GetScheduledTasksRepository();
            return scheduledStorage.Add(new ScheduledTasks
            {
                Task = _taskGuid,
                User = _userGuid,
                DateEnd = DateTime.Parse( EndTime),
                DateStart = StartTime
            });
        }

        //public void OutputScheduledTaskInDataGrid()
        //{
        //    var scheduledTaskService = new ScheduledTaskService(new XmlStorage());
        //    ScheduledTaskList = scheduledTaskService.GetTasks();
        //}

        //internal void OutputScheduledTaskInDataGrid(Guid userId)
        //{
        //    var scheduledTaskService = new ScheduledTaskService(new XmlStorage());
        //    var task = scheduledTaskService.GetTasks();
        //    ScheduledTaskList = task;
        //    TempScheduledTaskList = task;
        //}

    }
}
