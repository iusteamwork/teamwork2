﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using AppSerialization;
using AppSerialization.Service;
using AppSerialization.Storage;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using TaskManagmentApp.View;

namespace TaskManagmentApp.ViewModels
{
    class UserTaskViewModel:BaseViewModel
    {
        private List<RepresentUser> _userList;
        private List<RepresentUser> _taskUsers;
        private List<RepresentUserTask> _taskList;
        private Visibility _dataGridVisibility;
        private Visibility _messageVisibility;
        private Visibility _searchVisibility;

        public Visibility SearchVisibility
        {
            get { return _searchVisibility; }
            set { _searchVisibility = value; OnPropertyChanged(); }
        }

        public Visibility MessageVisibility
        {
            get { return _messageVisibility; }
            set { _messageVisibility = value; OnPropertyChanged();}
        }

        public Visibility DataGridVisibility
        {
            get { return _dataGridVisibility; }
            set { _dataGridVisibility = value; OnPropertyChanged(); }
        }

        public UserTaskViewModel()
        {
            DataGridVisibility = Visibility.Hidden;
            MessageVisibility = Visibility.Hidden;
            SearchVisibility = Visibility.Hidden;
        }

        public List<RepresentUserTask> TaskList
        {
            get { return _taskList; }
            set { _taskList = value; OnPropertyChanged(); }
        }

        public List<RepresentUser> TaskUsers
        {
            get { return _taskUsers; }
            set { _taskUsers = value; OnPropertyChanged();}
        }

        public List<RepresentUser> UserList
        {
            get { return _userList; }
            set { _userList = value; OnPropertyChanged();}
        }

        public void GetTaskUsers(Guid taskGuid)
        {
            if (taskGuid == Guid.Empty) return;
            var taskService = new TaskService(new FactoryService());
                  TaskUsers =taskService.GetUsersForTasks(taskGuid);

        }

        /// <summary>
        /// Detaches from the user's task
        /// </summary>
        /// <param name="userGuid"></param>
        /// <param name="taskGuid"></param>
        public void UnbindUser(Guid userGuid, Guid taskGuid)
        {
            if (taskGuid == Guid.Empty || userGuid == Guid.Empty) return;
            var userTasks = new UserTaskService(new FactoryService());
            if (!userTasks.DeleteUserTask(new UserTasks {Task = taskGuid, User = userGuid})) return;
            if (TaskUsers != null)
            {
                var taskUsers = TaskUsers;
                var representUser = taskUsers.FirstOrDefault(u => u.Id.Equals(userGuid));
                if (representUser == null) return;
                taskUsers.Remove(representUser);
                TaskUsers = null;
                TaskUsers = taskUsers;
            }
            else
            {
                var taskList = TaskList;
                var task=taskList.FirstOrDefault(t => t.Task.Equals(taskGuid));
                TaskList.Remove(task);
                TaskList = null;
                TaskList = taskList;
            }

        }

        internal void GetUsers()
        {
            var userStorage = new UserService(new FactoryService());
            UserList = userStorage.GetUsersList();

        }

        internal void BindUserTask(Guid userGuid, Guid taskGuid)
        {
            if (taskGuid == Guid.Empty || userGuid == Guid.Empty) return;
            var userTasks = new UserTaskRepository();
            var user = new UserTasks {User = userGuid, Task = taskGuid};
            if (userTasks.Add(user)) GetTaskUsers(taskGuid);
        }

        internal void SearchUser(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                GetUsers();
                return;
            }
            UserList = UserList.Where(p => p.Login.Contains(text)).ToList();
        }

        public void GetUserTasks(Guid id)
        {
            if (id == Guid.Empty) return;
            var userTaskService = new UserTaskService(new FactoryService());
            var userTasks = userTaskService.GetUserTasks(id);
            if (userTasks.Any())
            {
                DataGridVisibility = Visibility.Visible;
                SearchVisibility = Visibility.Visible;
                TaskList = userTasks;
            }
            else MessageVisibility = Visibility.Visible;
        }

        public void SearchTask(string text, Guid id)
        {
            if (TaskList != null)
            {
                if (string.IsNullOrEmpty(text))
                {
                    GetUserTasks(id);
                    return;
                }
                TaskList = TaskList.Where(p => p.Name.ToLower().Contains(text.ToLower())).ToList();
            }
        }
    }
}
