﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppSerialization.Storage;
using TaskManagmentApp.Data.SerializationTypes;

namespace TaskManagmentApp.ViewModels
{
    public class ActionViewModel: BaseViewModel
    {
        private List<UserLog> _userActionList;

        public List<UserLog> UserActionList
        {
            get { return _userActionList; }
            set { _userActionList = value;  OnPropertyChanged();}
        }

        public void GetActionList(Guid userGuid)
        {
            var logService = new UserLogDataStorage();
            UserActionList = logService.GetData().ToList();
        }
    }
}
