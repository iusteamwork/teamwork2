﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using AppSerialization;
using AppSerialization.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagmentApp.Data;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using Tools;

namespace TaskManagmentApp.ViewModels
{
    public class AdminViewModel : BaseViewModel
    {
        private List<string> _projectList;
        private List<RepresentTask> _taskList;
        private string _projectName;
        private List<RepresentUser> _userList;
        private string _pages;
        private int _currentPage;
        private string _page;
        private Visibility _gridVisibility;
        private string _selectedProject;
        private string _userName;

        public string CurrentUser
        {
            get { return _userName; }
            set { _userName ="Администратор:  "+value; OnPropertyChanged(); }
        }

        public string SelectedProject
        {
            get { return _selectedProject; }
            set { _selectedProject = value; OnPropertyChanged(); }
        }
        public Visibility GridVisibility
        {
            get { return _gridVisibility; }
            set { _gridVisibility = value; OnPropertyChanged(); }
        }
        // Navigation
        public string Pages
        {
            get { return _pages; }
            set { _pages = value; OnPropertyChanged();}
        }
        public string Page
        {
            get { return _page; }
            set { _page = value; OnPropertyChanged();}
        }
        public int CurrentPage
        {
            get { return _currentPage; }
            set { _currentPage = value; OnPropertyChanged(); }
        }
        /// <summary>
        /// Properties
        /// </summary>
        public List<string> ProjectList
        {
            get { return _projectList; }
            set { _projectList = value; OnPropertyChanged(); }
        }
        public List<RepresentTask> TaskList
        {
            get { return _taskList; }
            set { _taskList = value; OnPropertyChanged(); }
        }
        public List<RepresentUser> UserList
        {
            get { return _userList; }
            set { _userList = value; OnPropertyChanged(); }
        }
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; OnPropertyChanged(); }
        }
        public AdminViewModel()
        {
            CurrentPage = 1;
            GridVisibility = Visibility.Hidden;
            
        }

        
        public void ShowUserName(Guid userId)
        {
            if (userId != Guid.Empty)
            {
                var userService = new UserService(new FactoryService());
                CurrentUser=userService.GetUser(userId).Login;
            }
        }


        /// <summary>
        /// Conclusion projects in ComboBox
        /// </summary>
        public void OutputProjectInComboBox()
        {
            var projectService = new ProjectService(new FactoryService());
            ProjectList = projectService.TextualProjectListWithTaskCount();
        }

        public void GetProjectTask(string project)
        {
            if (!String.IsNullOrEmpty(project))
            {
                GridVisibility = Visibility.Visible;
                var projectService = new ProjectService(new FactoryService());
                var projectName = project.Split('-')[1];
                var projectId = projectService.GetProjectIdByName(projectName);
                // Проверка существования проекта
                if (projectId != Guid.Empty)
                {
                    var taskService = new TaskService(new FactoryService());
                    var taskList=taskService.GetProjectTasks(projectId).OrderByDescending(i=>i.CreateDate).ToList();
                    var pagination = new Paginator<RepresentTask>(taskList, 14);
                    TaskList = pagination.GetPageData(CurrentPage);
                    Pages = pagination.PageCount.ToString(CultureInfo.InvariantCulture);
                    Page = CurrentPage.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Delete selected task from all storages
        /// </summary>
        /// <param name="representTask"></param>
        /// <param name="project"></param>
        public void DeleteSelectedTask(RepresentTask representTask, string project)
        {
            if (representTask == null) return;
            var taskService = new TaskService(new FactoryService());
            if (!taskService.RemoveCompletely(representTask.Id)) return;
            RefreshProjectList(project,0);
        }


        /// <summary>
        /// Slava kostilyam
        /// </summary>
        /// <param name="project"></param>
        /// <param name="method"></param>
        public void RefreshProjectList(string project, int method)
        {
            GetProjectTask(project);
            var projectName = project.Split('-')[1];
            var projectList = ProjectList;
            var firstOrDefault = projectList.FirstOrDefault(p => p.Split('-')[1].Equals(projectName));
            if (firstOrDefault != null)
            {
                projectList.Remove(firstOrDefault);
                var soughtForProject = firstOrDefault.Split('-');
                if (method.Equals(1))
                {    
                    var number = int.Parse(soughtForProject[0].Substring(1, soughtForProject[0].Length - 3))+1;
                   projectList.Add("("+number+") -"+soughtForProject[1]);
                }
                if (method.Equals(0))
                {
                    var number = int.Parse(soughtForProject[0].Substring(1, soughtForProject[0].Length - 3)) - 1;
                    projectList.Add("(" + number + ") -" + soughtForProject[1]);
                }
                ProjectList = null;
                ProjectList = projectList;
            }
            SelectedProject = ProjectList.FirstOrDefault(p => p.Split('-')[1].Equals(projectName));
        }

        public void DeleteProject(string project)
        {
            if (String.IsNullOrEmpty(project)) return;
            var projectService = new ProjectService(new FactoryService());
            var projectName = project.Split('-')[1];
            var projectId = projectService.GetProjectIdByName(projectName);
            if (projectId == Guid.Empty) return;
            if (!projectService.RemoveCompletely(projectId)) return;
            var projectList = ProjectList;
            projectList.Remove(project);
            ProjectList = null;
            ProjectList = projectList;
            GridVisibility = Visibility.Hidden;
        }


        /// <summary>
        /// Add new project to storage
        /// </summary>
        /// <returns></returns>
        public OperationResult AddProject()
        {
            if (string.IsNullOrEmpty(ProjectName)) return new OperationResult{Message = "Имя проекта не должно быть пустым!",Status = false};
            var projectStorage = new FactoryService().GetProjectRepository();
            var projectData = projectStorage.GetData().Where(p => p.Name.Equals(ProjectName));
            if (projectData.Any()) return new OperationResult {Message = "Проект с таким именем уже существует",Status = false};
            var status = projectStorage.Add(new Project
            {
                Id = Guid.NewGuid(),
                Name = ProjectName,
                Date = DateTime.Now
            });
            if (!status) return new OperationResult {Message = "Проект успешно добавлен", Status = true};
            //Add new project to comobobox
            var projectList = ProjectList;
            projectList.Add("(0) - "+ProjectName);
            ProjectList = null;
            ProjectList = projectList;
            ProjectName = null;
            return new OperationResult {Message = "Проект успешно добавлен", Status = true};
        }


        public void GetUserList()
        {
            var userStorage = new UserService(new FactoryService());
            UserList = userStorage.GetUsersList().OrderByDescending(u=>u.Login).ToList();
        }

        public void SearchProject(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                OutputProjectInComboBox();
                return;
            }
            ProjectList = ProjectList.Where(p => p.ToLower().Contains(text.ToLower())).ToList();
        }

        public void SearchUser(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                GetUserList();
                return;
            }
            UserList = UserList.Where(p => p.Login.ToLower().Contains(text.ToLower())).OrderByDescending(u => u.Login).ToList();
        }

        public bool DeleteUser(RepresentUser userItem)
        {
            if (userItem != null)
            {
                var userStorage = new UserService(new FactoryService());
                if (userStorage.DeleteFromAll(userItem.Id))
                {
                    var userList = UserList;
                    userList.Remove(userItem);
                    UserList = null;
                    UserList = userList;
                }
            }
            return false;
        }

        public bool ChangeRole(RepresentUser userItem)
        {
            if (userItem == null) return false;
            var userService = new UserService(new FactoryService());
            if (!userService.ChangeUserRole(userItem)) return false;
            GetUserList();
            return true;
        }

        public void MoveBack(string projectName)
        {
            if (CurrentPage > 1 && CurrentPage <= int.Parse(Pages)) {CurrentPage = CurrentPage - 1; GetProjectTask(projectName);}
        }

        public void MoveForward(string projectName)
        {
            if (CurrentPage < int.Parse(Pages))
            {
                CurrentPage = CurrentPage + 1;
                GetProjectTask(projectName);
            }
        }

        public void OrderBy(string orderParam)
        {
            if (TaskList == null) return;
            switch (orderParam)
            {
                case "byTask":
                    TaskList = TaskList.OrderByDescending(i => i.Name).ToList();
                    break;
                case "byDate":
                    TaskList = TaskList.OrderByDescending(i => i.CreateDateFormat).ToList();
                    break;
                case "byEvaluation":
                    TaskList = TaskList.OrderByDescending(i => i.EvaluationTime).ToList();
                    break;
            }
        }

        public void SearchTask(string text,string project)
        {
            if (string.IsNullOrEmpty(text))
            {
                GetProjectTask(project);
                return;
            }
            TaskList = TaskList.Where(p => p.Name.ToLower().Contains(text.ToLower())).ToList();
        }

        public void OrderCompleteTasks(string tag)
        {
            if (TaskList != null)
            {
                switch (tag)
                {

                    case "byAll":
                        TaskList =
                            TaskList.OrderByDescending(i => i.ImplementationStatus == 1 && i.ImplementationStatus == 0)
                                .ToList();
                        break;
                    case "byComplete":
                        TaskList = TaskList.OrderByDescending(i => i.ImplementationStatus == 1).ToList();
                        break;
                    case "byUncomplete":
                        TaskList = TaskList.OrderByDescending(i => i.ImplementationStatus == 0).ToList();
                        break;
                }
            }
        }
    }
}
