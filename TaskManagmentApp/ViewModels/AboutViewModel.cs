﻿using System;
using TaskManagmentApp.Data.RepersentTypes;

namespace TaskManagmentApp.ViewModels
{
    class AboutViewModel : BaseViewModel
    {
        private string _project;
        private string _task;
        private string _about;
        private DateTime _deadLine;
        private string _manager;
        private string _dateStart;
        private string _dateEnd;
        private DateTime _timeDeleteTask;
        private string _deadLineFormat;
        public string Task
        {
            get { return _task; }
            set { _task = value; OnPropertyChanged(); }
        }
        public string About
        {
            get { return _about; }
            set { _about = value; OnPropertyChanged(); }
        }

        public string DeadLineStringFormat
        {
            get { return _deadLineFormat; }
            set { _deadLineFormat = value; OnPropertyChanged(); }
        }

        public DateTime DeadLine
        {
            get { return _deadLine; }
            set { _deadLine = value; OnPropertyChanged(); }
        }
        public string Manager
        {
            get { return _manager; }
            set { _manager = value; OnPropertyChanged(); }
        }
        public string Project
        {
            get { return _project; }
            set { _project = value; OnPropertyChanged(); }
        }
        public string DateStart
        {
            get { return _dateStart; }
            set { _dateStart = value; OnPropertyChanged(); }
        }
        public string DateEnd
        {
            get { return _dateEnd; }
            set { _dateEnd = value; OnPropertyChanged(); }
        }
        public DateTime TimeDeleteTask
        {
            get { return _timeDeleteTask; }
            set { _timeDeleteTask = value; OnPropertyChanged(); }
        }

        public void ShowInfo(RepresentTask task)
        {
            if (task == null) return;
            Project = task.ProjectName;
            Task = task.Name;
            About = task.About;
            Manager = task.ManagerName;
            DeadLine = task.DeadLine;
            DeadLineStringFormat = DeadLine.ToString("dd MMMM yyyy");
        }

        public void ShowInfo(RepresentScheduledTask scheduledTask)
        {
            if (scheduledTask == null) return;
            Project = scheduledTask.ProjectName;
            Task = scheduledTask.Name;
            About = scheduledTask.About;
            Manager = scheduledTask.ManagerLogin;
            DeadLine = scheduledTask.DeadLine;
            DeadLineStringFormat = DeadLine.ToString("dd MMMM yyyy");
            DateStart = scheduledTask.DateStart.ToString("H:mm:ss");
            DateEnd = scheduledTask.DateEnd.ToString("H:mm:ss");
        }

        public void ShowInfo(RepresentDeleteTask deletedTask)
        {
            if (deletedTask == null) return;
            Project = deletedTask.ProjectName;
            Task = deletedTask.Name;
            About = deletedTask.About;
            DeadLineStringFormat = DeadLine.ToString("dd MMMM yyyy");
            Manager = deletedTask.ManagerName;
            DeadLine = deletedTask.DeadLine;
            TimeDeleteTask = deletedTask.TimeDeleteTask;
        }

        internal void ShowProjectTaskInfo(RepresentTask task)
        {
            if (task == null) return;
            Project = task.ProjectName;
            Task = task.Name;
            About = task.About;
            DeadLine = task.DeadLine;
            DeadLineStringFormat = DeadLine.ToString("dd MMMM yyyy"); 
        }
    }
}
