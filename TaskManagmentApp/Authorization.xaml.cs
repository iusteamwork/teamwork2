﻿using AppSerialization;
using AppSerialization.Service;
using System;
using System.Windows;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp
{
    /// <summary>
    /// Interaction logic for Authorization.xaml
    /// </summary>
    public partial class Authorization
    {
        private readonly AuthorizationViewModel _authViewModel;
        public Authorization()
        {
            InitializeComponent();
            DataContext = new AuthorizationViewModel();
            _authViewModel = DataContext as AuthorizationViewModel;
            //var service = new TokenService(new FactoryService());
            //service.AddToken(new Guid("db70d30e-9785-498b-ba74-02c5ec3cb4b2"));
        }

        private void AuthClick(object sender, RoutedEventArgs e)
        {

            var user= _authViewModel.AuthorizeUser();
            if (user == null) return;
            if (user.User == Guid.Empty) return;
            switch (user.Role)
            {
                case 0:
                {
                    new MainWindow(user.User).Show();
                    Close();
                }
                break;
                case 1:
                {
                    new AdminWindow(user.User).Show();
                    Close();
                }
                break;
            }
        }

        private void OpenAdminWindow(object sender, RoutedEventArgs e)
        {
            new AdminWindow(Guid.Parse("db70d30e-9785-498b-ba74-02c5ec3cb4b2")).Show();
            Close();          
        }
    }
}