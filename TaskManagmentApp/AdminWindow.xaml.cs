﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Elysium;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.View;
using TaskManagmentApp.View.Admin;
using TaskManagmentApp.ViewModels;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace TaskManagmentApp
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow
    {
        private readonly Guid _adminGuid;
        private readonly AdminViewModel _adminViewModel;
        private RepresentTask _representTask;
        private string _projectName;
        private RepresentUser _userItem;
        private AdminWindow() { }

        public AdminWindow(Guid adminGuid)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            _adminGuid = adminGuid;
            _adminGuid = Guid.Parse("61d0deee-d0bf-45ea-8d09-ffee7fd09a7e");
            DataContext = new AdminViewModel();
            _adminViewModel = DataContext as AdminViewModel;
            Loaded += AdminWindow_Loaded;
            SizeChanged += AdminWindow_SizeChanged;
        }

        void AdminWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (_adminGuid.Equals(Guid.Empty))
            {
                MessageBox.Show("Admin Guid is empty");
                Close();
            }
            _adminViewModel.OutputProjectInComboBox();
            _adminViewModel.GetUserList();
            _adminViewModel.ShowUserName(_adminGuid);
        }
        void AdminWindow_SizeChanged(object sender, RoutedEventArgs e)
        {
            if (Width <= 1000)
            {
                Info.Visibility = Visibility.Hidden;
                Edit.Visibility = Visibility.Hidden;
                Pin.Visibility = Visibility.Hidden;
                SlideItem.Visibility = Visibility.Visible;
            }
            else
            {
                Info.Visibility = Visibility.Visible;
                Edit.Visibility = Visibility.Visible;
                Pin.Visibility = Visibility.Visible;
                SlideItem.Visibility = Visibility.Hidden;
            }
        }
        private void ShowMore(object sender, RoutedEventArgs e)
        {
            if (_representTask != null)
                new AboutProjectTask(_representTask).ShowDialog();
            else MessageBox.Show("Выберете задачу");
        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            if (_userItem == null) MessageBox.Show("Выюберете пользователя");
            if (MessageBox.Show("Вы уверены, что хотите удалить пользователя?", "Удаление пользователя", MessageBoxButton.YesNo, MessageBoxImage.Question) !=
                MessageBoxResult.Yes) return;
            if (_adminViewModel.DeleteUser(_userItem)) MessageBox.Show("Пользователь успешно удалён");
        }

        private void DeleteTask(object sender, RoutedEventArgs e)
        {
            if (_representTask == null){ MessageBox.Show("Выберете задачу"); return;}
            if (MessageBox.Show("Вы уверены, что хотите удалить задачу?", "Удаление задачи", MessageBoxButton.YesNo, MessageBoxImage.Question) !=
                MessageBoxResult.Yes) return;
            _adminViewModel.DeleteSelectedTask(_representTask, _projectName);
        }

        private void DeleteProject(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(_projectName))
            {
                if (
                    MessageBox.Show("Вы уверены, что хотите удалить проект?", "Удаление проекта", MessageBoxButton.YesNo,
                        MessageBoxImage.Question) !=
                    MessageBoxResult.Yes) return;
                if (!String.IsNullOrEmpty(_projectName)) _adminViewModel.DeleteProject(_projectName);
            }
            else MessageBox.Show("Выберете проект");
        }

        private void AddProject(object sender, RoutedEventArgs e)
        {
            var result = _adminViewModel.AddProject();
            if (!result.Status) MessageBox.Show(result.Message);
        }

        private void AddTask(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(_projectName)) new AddTask(_projectName,_adminGuid, _adminViewModel).ShowDialog();
            else MessageBox.Show("Выберете проект");
        }

        private void AddUser(object sender, RoutedEventArgs e)
        {
            new AddUser(_adminViewModel).ShowDialog();
        }

        private void ProjectChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox == null || listBox.SelectedItem == null) return;
            var project = listBox.SelectedItem.ToString();
            _projectName = project;
            _adminViewModel.GetProjectTask(project);
        }

        private void TaskChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _representTask = dataGrid.SelectedItem as RepresentTask;
        }

        private void TaskUsers(object sender, RoutedEventArgs e)
        {
            if (_representTask != null) new UserTask(_representTask, _adminViewModel).ShowDialog();
            else MessageBox.Show("Выберете задачу");
        }

        private void EditTask(object sender, RoutedEventArgs e)
        {
            if (_representTask != null) new EditTask(_representTask,_adminGuid, _adminViewModel, _projectName).ShowDialog();
            else MessageBox.Show("Выберете задачу");   
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as WatermarkTextBox;
            if (textBox == null) return;
            _adminViewModel.SearchProject(textBox.Text);
        }

        private void UserTask(object sender, RoutedEventArgs e)
        {
            if (_userItem != null) new TaskList(_userItem.Id).ShowDialog();
        }

        private void UserChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _userItem = dataGrid.SelectedItem as RepresentUser;
        }

        private void SearchUserChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as WatermarkTextBox;
            if (textBox == null) return;
            _adminViewModel.SearchUser(textBox.Text);
        }

        private void UserActions(object sender, RoutedEventArgs e)
        {
            if (_userItem != null) new UserActions(_userItem.Id).ShowDialog();
        }


        private void Back(object sender, RoutedEventArgs e)
        {
            _adminViewModel.MoveBack(_projectName);
        }

        private void MoveForward(object sender, RoutedEventArgs e)
        {
            _adminViewModel.MoveForward(_projectName);
        }

        private void DataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            var da = new DoubleAnimation
            {
                From = 0,
                To = 1,
                RepeatBehavior = new RepeatBehavior(1),
                Duration = new Duration(TimeSpan.FromSeconds(1))
            };
            //da.AutoReverse = true;
            //da.RepeatBehavior = RepeatBehavior.Forever;
            e.Row.BeginAnimation(OpacityProperty, da);
        }

        private void OrderByChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _adminViewModel!=null) _adminViewModel.OrderBy(comboBox.Tag.ToString());
        }

        private void SearchTask(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as WatermarkTextBox;
            if (textBox == null) return;
            _adminViewModel.SearchTask(textBox.Text,_projectName);
        }


        private void TaskGridDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_representTask == null) return;
            new AboutProjectTask(_representTask).Show();
        }

        private void OrderCompleteUncompleteTasks(object sender, SelectionChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(_projectName)) return;
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _adminViewModel!=null) _adminViewModel.OrderCompleteTasks(comboBox.Tag.ToString());
        }

        private void MenuItemChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox == null || comboBox.Tag == null) return;
            switch(comboBox.Tag.ToString())
            {
                case "SwitchUser":
                    new Authorization().Show();
                    Close();
                    break;
            }
        }
    }
}
