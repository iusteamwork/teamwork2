﻿using AppSerialization;
using AppSerialization.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp
{
    public class ScheduledTaskToControlConverter : IValueConverter
    {
        private TaskViewModel _task = new TaskViewModel();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WorkingTimeClass date = (WorkingTimeClass)value;

            SolidColorBrush brush;


            if (DateTime.Now < date.StartDate) return new SolidColorBrush(Colors.WhiteSmoke);
            if (DateTime.Now > date.EndDate) return new SolidColorBrush(Color.FromRgb(219, 255, 255));

            var diff = date.EndDate.Subtract(date.StartDate).TotalHours / 10;

            if (DateTime.Now.Subtract(date.StartDate).TotalHours <= diff * 8)
                return brush = new SolidColorBrush(Color.FromRgb(199, 255, 255));
            if (DateTime.Now.Subtract(date.StartDate).TotalHours > diff * 8)
                return brush = new SolidColorBrush(Color.FromRgb(209, 255, 255));

            else return new SolidColorBrush(Colors.White);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}