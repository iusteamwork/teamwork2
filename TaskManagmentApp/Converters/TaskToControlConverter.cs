﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp
{
    class TaskToControlConverter : IValueConverter
    {
        private TaskViewModel _task = new TaskViewModel();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var deadLine = (DateTime)value;
            SolidColorBrush brush;
            if (DateTime.Now.Date <= deadLine.Date)
                return brush = new SolidColorBrush(Color.FromRgb(199, 255, 255));
            else return brush = new SolidColorBrush(Color.FromRgb(219, 255, 255));
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
