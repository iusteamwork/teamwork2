﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TaskManagmentApp
{
    public class Dates
    {
        public DateTime Date { get; set; }
        public string Overlay { get; set; }
        public Dates(DateTime date, string overlay)
        {
            Date = date;
            Overlay = overlay;
        }
    }

    public class Overlays
    {
        public readonly BitmapImage _bitMap;
        public readonly ImageBrush _brush;
        public string Id { get; set; }
        public Overlays(string id, string fileName)
        {
            Id = id;
            _bitMap = CalendarBackground.BitmapImage(fileName, out _brush);
        }
    }

    internal class CalendarBackground
    {
        private readonly List<Dates> _dateList = new List<Dates>();
        private readonly List<Overlays> _overlayList = new List<Overlays>();
        private Calendar _calendar;

        public CalendarBackground()
        {

        }

        public CalendarBackground(Calendar calendar)
        {
            _calendar = calendar;
        }

        public void ClearDates()
        {
            _dateList.Clear();
        }

        public void AddOverlay(string id, string fileName)
        {
            _overlayList.Add(new Overlays(id, fileName));
        }

        public void AddDate(DateTime date, string overlay)
        {
            _dateList.Add(new Dates(date, overlay));
        }

        public void RemoveDate(DateTime date, string overlay)
        {
            _dateList.Remove(new Dates(date, overlay));
        }

        public ImageBrush GetBackground()
        {
            DateTime displayDate = _calendar.DisplayDate;
            var firstDayOfMonth = new DateTime(displayDate.Year, displayDate.Month, 1);
            var dayOfWeek = (int)firstDayOfMonth.DayOfWeek;
            if (dayOfWeek == 0) dayOfWeek = 7;
            if (dayOfWeek == (int)_calendar.FirstDayOfWeek) dayOfWeek = 8;
            if (_calendar.FirstDayOfWeek == DayOfWeek.Sunday) dayOfWeek += 1;
            DateTime firstDate = firstDayOfMonth.AddDays(-((Double)dayOfWeek) + 1);

            Debug.WriteLine("displayd date    {0} ", displayDate);
            Debug.WriteLine("firstdayofmonth  {0} ", firstDayOfMonth);
            Debug.WriteLine("dayofweek        {0} ", dayOfWeek);
            Debug.WriteLine("firstdate        {0} ", firstDate);

            var rtBitmap = new RenderTargetBitmap(140, 160, 83, 86, PixelFormats.Default);

            var drawVisual = new DrawingVisual();
            using (DrawingContext dc = drawVisual.RenderOpen())
            {
                var backgroundBrush = new LinearGradientBrush();
                backgroundBrush.StartPoint = new Point(0.5, 0);
                backgroundBrush.EndPoint = new Point(0.5, 1);
                backgroundBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFE4EAF0"), 0.0));
                backgroundBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFECF0F4"), 0.16));
                backgroundBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFCFCFD"), 0.16));
                backgroundBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFFFFFF"), 1));

                dc.DrawRectangle(backgroundBrush, null, new Rect(0, 0, rtBitmap.Width, rtBitmap.Height));
            }
            rtBitmap.Render(drawVisual);

            using (DrawingContext dc = drawVisual.RenderOpen())
            {
                for (int y = 0; y < 6; y++)
                    for (int x = 0; x < 7; x++)
                    {
                        int xpos = x * 15 + 26;
                        int ypos = y * 16 + 58;
                        if (y == 2) ypos -= 1;
                        if (y == 3) ypos -= 2;
                        if (y == 4) ypos -= 2;
                        if (y == 5) ypos -= 3;

                        foreach (string overlayId in _dateList.Where(c => c.Date == firstDate).Select(c => c.Overlay))
                        {
                            if (overlayId != null)
                            {
                                Overlays overlays = _overlayList.Where(c => c.Id == overlayId).FirstOrDefault();
                                try
                                {
                                    dc.DrawRectangle(overlays._brush, null, new Rect(xpos, ypos, overlays._bitMap.Width, overlays._bitMap.Height));
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                        firstDate = firstDate.AddDays(1);
                    }
            }
            rtBitmap.Render(drawVisual);

            var brush = new ImageBrush(rtBitmap);
            return brush;
        }

        public static BitmapImage BitmapImage(string fileName, out ImageBrush imageBrush)
        {
            var overlay = new BitmapImage(new Uri(fileName, UriKind.Relative));
            imageBrush = new ImageBrush();
            imageBrush.ImageSource = overlay;
            imageBrush.Stretch = Stretch.Uniform;
            imageBrush.TileMode = TileMode.None;
            imageBrush.AlignmentX = AlignmentX.Center;
            imageBrush.AlignmentY = AlignmentY.Center;
            imageBrush.Opacity = 0.75;
            return overlay;
        }
    }
}
