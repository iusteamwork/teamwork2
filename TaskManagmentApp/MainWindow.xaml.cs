﻿using AppSerialization;
using AppSerialization.Service;
using AppSerialization.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using TaskManagmentApp.View;
using TaskManagmentApp.ViewModels;


namespace TaskManagmentApp
{
    public delegate void LoadData(Guid userGuid);


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        //User id for current window
        private readonly Guid _user;
        private readonly TaskViewModel _taskViewModel;
        private readonly UserLogViewModel _viewLog = new UserLogViewModel();
        private RepresentTask _representTask;
        private RepresentScheduledTask _scheduledTask;
        private RepresentDeleteTask _deletedTask;

        private readonly CalendarBackground _taskBackground;
        private readonly CalendarBackground _scheduledTaskBackground;

        StackPanel stackPanel = new StackPanel();

        readonly Guid _curentUser;

        //Selected Project
        private string _projectName;
        public MainWindow(Guid user)
        {
            _user = user;
            InitializeComponent();
            DataContext = new TaskViewModel();
            _taskViewModel = DataContext as TaskViewModel;
            Loaded += MainWindow_Loaded;
            _curentUser = user;
            listBox.ItemsSource = _viewLog.ShowLogs(_curentUser);

            TasksCalendar.IsTodayHighlighted = false;
            ScheduledTasksCalendar.IsTodayHighlighted = false;

            _taskBackground = new CalendarBackground(TasksCalendar);
            _scheduledTaskBackground = new CalendarBackground(ScheduledTasksCalendar);

            AddImage();
            AddDateToTasks();
            AddDateToScheduledTasks();

            TasksCalendar.Background = _taskBackground.GetBackground();
            ScheduledTasksCalendar.Background = _scheduledTaskBackground.GetBackground();

            TasksCalendar.DisplayDateChanged += TaskCalendarOnDisplayDateChanged;
            ScheduledTasksCalendar.DisplayDateChanged += ScheduledTaskCalendarOnDisplayDateChanged;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var loadData = new LoadData(_taskViewModel.OutputTaskInDataGrid);
            loadData += _taskViewModel.OutputProjectInComboBox;
            //loadData += _taskViewModel.OutputTaskInDataGrid;
            //loadData += _taskViewModel.OutputScheduledTaskInDataGrid;
            loadData += _taskViewModel.OutputDeletedTaskInDataGrid;
            loadData += _taskViewModel.ShowUserName;
            loadData(_user);
            _taskViewModel.Init();
            _taskViewModel.OutProjectTasksForUser("Все проекты", _curentUser);
            _taskViewModel.OutProjectScheduledTasks("Все проекты", _curentUser);
        }


        public void AddImage()
        {
            _taskBackground.AddOverlay("deadLine", "deadLine.png");
            _scheduledTaskBackground.AddOverlay("active", "active.png");
        }

        public void AddDateToTasks()
        {
            var taskService = new TaskService(new FactoryService());
            var task = taskService.GetUserTasks(_curentUser).Select(c => c.DeadLine);
            foreach (var item in task)
            {
                _taskBackground.AddDate(item.Date, "deadLine");

            }
        }

        public void AddDateToScheduledTasks()
        {
            var scheduledTaskService = new ScheduledTaskService(new FactoryService());
            var scheduledTask = scheduledTaskService.GetTasksByUser(_curentUser).Select(c => c);

            var activeDates = new List<DateTime>();

            foreach (var item in scheduledTask)
            {
                while (item.DateStart < item.DateEnd)
                {
                    item.DateStart = item.DateStart.AddDays(1);
                    activeDates.Add(item.DateStart.AddDays(-1));
                }
            }

            foreach (var item in activeDates)
            {
                _scheduledTaskBackground.AddDate(item.Date, "active");
            }
        }

        private void TaskCalendarOnDisplayDateChanged(object sender, CalendarDateChangedEventArgs calendarDateChangedEventsArgs)
        {
            TasksCalendar.Background = _taskBackground.GetBackground();
        }

        private void ScheduledTaskCalendarOnDisplayDateChanged(object sender, CalendarDateChangedEventArgs calendarDateChangedEventsArgs)
        {
            ScheduledTasksCalendar.Background = _scheduledTaskBackground.GetBackground();
        }

        private void PlanTask(object sender, RoutedEventArgs e)
        {
            if (_representTask != null)
            {
                //Check scheduled this task, for current user
                var checkTask = _taskViewModel.CheckScheduledTask(_representTask.Id, _user);
                //If task true, task was scheduled
                if (checkTask) MessageBox.Show("Эта задача уже распланирована");
                else
                {
                    _viewLog.SerializeLog(_curentUser, "Пользователь распланировал задачу.", _representTask.Name, "Resources/tasks.png");
                    listBox.ItemsSource = _viewLog.ShowLogs(_curentUser);
                    new ScheduleTaskWindow(_representTask, _user).ShowDialog();
                }
                _taskViewModel.OutputScheduledTaskInDataGrid(_user);
            }
            else MessageBox.Show("Выберите задачу ");
        }


        private void TaskDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                _representTask = dataGrid.SelectedItem as RepresentTask;
                //Подгрузка кнопок для выбраной задачи
                _taskViewModel.ShowControlsForSelectedTask(_representTask, _curentUser);
            }
        }

        private void ScheduledTaskDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _scheduledTask = dataGrid.SelectedItem as RepresentScheduledTask;
        }

        private void DeletedTaskDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _deletedTask = dataGrid.SelectedItem as RepresentDeleteTask;
        }

        private void TaskAbout(object sender, RoutedEventArgs e)
        {
            if (_representTask != null) new TaskAbout(_representTask).ShowDialog();
            else MessageBox.Show("Выберите задачу");
        }

        private void DeletedTaskAbout(object sender, RoutedEventArgs e)
        {
            if (_deletedTask != null) new DeletedTaskAbout(_deletedTask).ShowDialog();
            else MessageBox.Show("Выберите задачу");
        }

        private void ScheduledTaskAbout(object sender, RoutedEventArgs e)
        {
            if (_scheduledTask != null) new ScheduledTaskAbout(_scheduledTask).ShowDialog();
            else MessageBox.Show("Выберите задачу");
        }

        private void TimeStart(object sender, RoutedEventArgs e)
        {
            if(_representTask!=null) _taskViewModel.Timer(Timer, _representTask);
        }

        private void CompleteTask(object sender, RoutedEventArgs e)
        {
            if (_scheduledTask != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите завершить задачу?", "Завершение задачи", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                _viewLog.SerializeLog(_scheduledTask.User, "Пользователь завершил задачу.",  _scheduledTask.Name, "Resources/tasks.png");
                listBox.ItemsSource = _viewLog.ShowLogs(_curentUser);
                _taskViewModel.CompletedTask(_scheduledTask, _curentUser);
                _taskViewModel.OutputDeletedTaskInDataGrid(_user);
            }
            else MessageBox.Show("Выберите задачу");
        }

        private void RestoreTask(object sender, RoutedEventArgs e)
        {
            if (_deletedTask != null)
            {
                _viewLog.SerializeLog(_curentUser, "Пользователь восстановил задачу.", _deletedTask.Name, "Resources/tasks.png");
                stackPanel.Children.Clear();
                listBox.ItemsSource = _viewLog.ShowLogs(_curentUser);
                _taskViewModel.RestoreTask(_deletedTask);
                _taskViewModel.OutputTaskInDataGrid(_user);
                _taskViewModel.OutputScheduledTaskInDataGrid(_user);
                _taskViewModel.OutputDeletedTaskInDataGrid(_user);
            }
            else MessageBox.Show("Выберите задачу");
        }

        private void DeleteTask(object sender, RoutedEventArgs e)
        {
            if (_deletedTask != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите удалить задачу?", "Удаление задачи", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                _viewLog.SerializeLog(_curentUser, "Пользователь удалил задачу.", _deletedTask.Name, "Resources/trash.png");
                listBox.ItemsSource = _viewLog.ShowLogs(_curentUser);
                _taskViewModel.DeletedTask(_deletedTask);
                _taskViewModel.OutputDeletedTaskInDataGrid(_user);
            }
            else MessageBox.Show("Выберите задачу");
        }

        private void ClearAll(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить все задачи?", "Удаление задач", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;
            _taskViewModel.DeletedAll(_deletedTask);
            _taskViewModel.OutputDeletedTaskInDataGrid(_user);
        }


        private void WatermarkTasksFilter(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as Xceed.Wpf.Toolkit.WatermarkTextBox;
            if (textBox == null)
            {
                _taskViewModel.FilterTasks();
                return;
            }
            _taskViewModel.WatermarkFilterTasks(textBox.Text);
        }

        private void FilterTasks(object sender, SelectionChangedEventArgs e)
        {
            _taskViewModel.FilterTasks();
        }

        private void WatermarkScheduledTasksFilter(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as Xceed.Wpf.Toolkit.WatermarkTextBox;
            if (textBox == null)
            {
                _taskViewModel.FilterScheduledTasks();
                return;
            }
            _taskViewModel.WatermarkFilterScheduledTasks(textBox.Text);
        }

        private void FilterScheduledTasks(object sender, SelectionChangedEventArgs e)
        {
            _taskViewModel.FilterScheduledTasks();
        }

        private void WatermarkDeletedTasksFilter(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as Xceed.Wpf.Toolkit.WatermarkTextBox;
            if (textBox == null)
            {
                _taskViewModel.FilterDeletedTasks();
                return;
            }
            _taskViewModel.WatermarkFilterDeletedTasks(textBox.Text);
        }

        //Главное меню
        private void MenuItemChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null &&  comboBox.Tag!=null)
            {
                switch (comboBox.Tag.ToString())
                {
                    case "SwitchUser":
                        new Authorization().Show();
                        Close();
                        break;
                }
            }
        }

        //Сортирует задачи для текущего проекта 
        private void OrderCompleteUncompleteTasks(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _taskViewModel != null) _taskViewModel.OutCompleteTasks(comboBox.Tag.ToString());
        }

        //Загружает список задач для выбраного проекта
        private void ProjectChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (combo == null) return;
            _projectName = combo.SelectedItem.ToString();
            _taskViewModel.OutProjectTasksForUser(_projectName, _user);
        }

        private void OrderTaskDataGrid(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _taskViewModel != null) _taskViewModel.OrderTaskList(comboBox.Tag.ToString());
        }

        private void OrderScheduledCompleteTasks(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _taskViewModel != null) _taskViewModel.OutScheduledCompleteTasks(comboBox.Tag.ToString());
        }

        private void OrderScheduledDataGrid(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ComboBox;
            if (item == null) return;
            var comboBox = item.SelectedItem as StackPanel;
            if (comboBox != null && _taskViewModel != null) _taskViewModel.OrderScheduledList(comboBox.Tag.ToString());
        }

        private void ScheduledProjectChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (combo == null) return;
            _projectName = combo.SelectedItem.ToString();
            _taskViewModel.OutProjectScheduledTasks(_projectName, _user);
        }
    }
}
