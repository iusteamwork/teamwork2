﻿using System;
using System.Windows;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for ScheduleTaskWindow.xaml
    /// </summary>
    public partial class ScheduleTaskWindow
    {
        private readonly RepresentTask _representTask;

        public ScheduleTaskWindow(RepresentTask representTask, Guid userGuid)
        {
            _representTask = representTask;
            InitializeComponent();
            DataContext = new ScheduleTaskViewModel(userGuid);
            Loaded += TaskAbout_Loaded;
            InitializeComponent();
        }

        private void TaskAbout_Loaded(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ScheduleTaskViewModel;
            if (context != null) context.ShowInfo(_representTask);
        }

        //Schedule 
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ScheduleTaskViewModel;
            //if (context.EndTime.CompareTo(context.DeadLine) == 1)
            //    {
            //        MessageBox.Show("При планировании возникла ошибка. \nДата окончания превышает сроки сдачи задачи.", "Ошибка планирования");
            //        return;
            //    }
            if (context != null && context.ScheduleTheTask())
            {
                 MessageBox.Show("Задача запланирована");
                Close();
            }
            else MessageBox.Show("При планировании возникла ошибка", "Ошибка планирования");
        }
    }
}
