﻿using System;
using System.Windows;
using System.Windows.Controls;
using TaskManagmentApp.ViewModels;
using Xceed.Wpf.Toolkit;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for AddTask.xaml
    /// </summary>
    public partial class AddTask
    {
        private readonly string _projectName;
        private readonly Guid _managerGuid;
        private readonly AdminViewModel _adminViewModel;
        private readonly AddEditTaskViewModel _viewModel;
        private AddTask() { }
        public AddTask(string projectName, Guid managerGuid, AdminViewModel adminViewModel)
        {
            _projectName = projectName;
            _managerGuid = managerGuid;
            _adminViewModel = adminViewModel;
            InitializeComponent();
            DataContext = new AddEditTaskViewModel();
            _viewModel = DataContext as AddEditTaskViewModel;
        }

        private void AddNewTask(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(_projectName) || _managerGuid == Guid.Empty) return;
            if (!_viewModel.AddTask(_projectName, _managerGuid)) return;
            _adminViewModel.GetProjectTask(_projectName);
            _adminViewModel.RefreshProjectList(_projectName, 1);
            Close();
        }

    }
}
