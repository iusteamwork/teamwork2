﻿using System.Windows;
using System.Windows.Controls;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.Data.SerializationTypes;
using TaskManagmentApp.ViewModels;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for UserTask.xaml
    /// </summary>
    public partial class UserTask
    {
        private readonly RepresentTask _representTask;
        private readonly AdminViewModel _adminViewModel;
        private readonly UserTaskViewModel _userViewModel;

        //Changed items

        private RepresentUser _selectedUser;
        private RepresentUser _taskUser;

        private UserTask()
        {
            
        }

        public UserTask(RepresentTask representTask, AdminViewModel adminViewModel)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            _representTask = representTask;
            _adminViewModel = adminViewModel;
            DataContext = new UserTaskViewModel();
            _userViewModel = DataContext as UserTaskViewModel;
            Loaded += UserTask_Loaded;
        }

        void UserTask_Loaded(object sender, RoutedEventArgs e)
        {
            _userViewModel.GetTaskUsers(_representTask.Id);
            _userViewModel.GetUsers();
        }

        private void Nex(object sender, RoutedEventArgs e)
        {

        }

        private void Previous(object sender, RoutedEventArgs e)
        {

        }

        private void TaskUserChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _taskUser= dataGrid.SelectedItem as RepresentUser;
        }

        private void UserListChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null) _selectedUser = dataGrid.SelectedItem as RepresentUser;
        }


        private void UnbindUser(object sender, RoutedEventArgs e)
        {
            if (_taskUser != null)
            {
                if (
                    MessageBox.Show("Вы уверены, что хотите отвязать этого пользователя?", "Потверждение", MessageBoxButton.YesNo,
                        MessageBoxImage.Question) !=
                    MessageBoxResult.Yes) return;
                _userViewModel.UnbindUser(_taskUser.Id, _representTask.Id);
                _adminViewModel.GetProjectTask("() -" + _representTask.ProjectName);
            }
            else MessageBox.Show("Выберете пользователя");
        }

        private void BindUser(object sender, RoutedEventArgs e)
        {
            if (_selectedUser != null)
            {
                _userViewModel.BindUserTask(_selectedUser.Id, _representTask.Id);
                _adminViewModel.GetProjectTask("() -"+_representTask.ProjectName);
            }
            else MessageBox.Show("Выберете пользователя");
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as WatermarkTextBox;
            if (textBox == null) return;
            _userViewModel.SearchUser(textBox.Text);
        }
    }
}
