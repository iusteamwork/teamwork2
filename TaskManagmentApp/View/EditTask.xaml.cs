﻿using System;
using System.Windows;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for EditTask.xaml
    /// </summary>
    public partial class EditTask
    {
        private readonly RepresentTask _representTask;
        private readonly Guid _guid;
        private readonly AdminViewModel _adminViewModel;
        private readonly string _projectName;
        private readonly AddEditTaskViewModel _viewModel;

        private EditTask() { }
        public EditTask(RepresentTask representTask, Guid guid, AdminViewModel adminViewModel, string projectName)
        {
            _representTask = representTask;
            _guid = guid;
            _adminViewModel = adminViewModel;
            _projectName = projectName;
            InitializeComponent();
            DataContext = new AddEditTaskViewModel();
            _viewModel=DataContext as AddEditTaskViewModel;
            Loaded += EditTask_Loaded;
        }

        void EditTask_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel.Task = _representTask.Name;
            _viewModel.About = _representTask.About;
            _viewModel.Evaluation = _representTask.EvaluationTime;
            _viewModel.DeadLine = _representTask.DeadLine;
        }

        private void SaveChanges(object sender, RoutedEventArgs e)
        {
            if (!_viewModel.EditTask(_representTask, _guid)) return;
            MessageBox.Show("Задача успешно обновлена");
            _adminViewModel.GetProjectTask(_projectName);
            Close();
        }

    }
}
