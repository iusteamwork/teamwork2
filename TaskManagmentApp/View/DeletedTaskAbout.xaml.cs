﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for DeletedTaskAbout.xaml
    /// </summary>
    public partial class DeletedTaskAbout
    {
        private readonly RepresentDeleteTask _deletedTask;

        public DeletedTaskAbout(RepresentDeleteTask deletedTask)
        {
            _deletedTask = deletedTask;
            InitializeComponent();
            DataContext = new AboutViewModel();
            Loaded += DeletedTaskAbout_Loaded;
        }
        void DeletedTaskAbout_Loaded(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AboutViewModel;
            if (context != null) context.ShowInfo(_deletedTask);
        }
    }
}
