﻿using System;
using System.Windows;
using System.Windows.Controls;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;
using Xceed.Wpf.Toolkit;
using MessageBox = System.Windows.MessageBox;

namespace TaskManagmentApp.View.Admin
{
    /// <summary>
    /// Interaction logic for TaskList.xaml
    /// </summary>
    public partial class TaskList
    {
        private readonly Guid _id;
        private RepresentUserTask _representTask;
        private readonly UserTaskViewModel _userTaskViewModel;

        public TaskList(Guid id)
        {
            _id = id;
            InitializeComponent();
            DataContext = new UserTaskViewModel();
            _userTaskViewModel = DataContext as UserTaskViewModel;
            Loaded += TaskList_Loaded;
        }

        void TaskList_Loaded(object sender, RoutedEventArgs e)
        {
            _userTaskViewModel.GetUserTasks(_id);
        }

        private void DeleteTask(object sender, RoutedEventArgs e)
        {
            if (_representTask != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите удалить задачу?", "Удаление задачи", MessageBoxButton.YesNo, MessageBoxImage.Question) !=
            MessageBoxResult.Yes) return;
                _userTaskViewModel.UnbindUser(_id, _representTask.Task);
            }
            else Xceed.Wpf.Toolkit.MessageBox.Show("Выберете задачу");
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as WatermarkTextBox;
            if (textBox == null) return;
            _userTaskViewModel.SearchTask(textBox.Text, _id);
        }

        private void TaskChanged(object sender, SelectionChangedEventArgs e)
        {
            var dg = sender as DataGrid;
            if (dg != null) _representTask = dg.SelectedItem as RepresentUserTask;
        }
    }
}
