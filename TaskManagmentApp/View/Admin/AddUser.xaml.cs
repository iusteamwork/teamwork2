﻿using System.Windows;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View.Admin
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser
    {
        private readonly AdminViewModel _adminViewModel;

        public AddUser(AdminViewModel adminViewModel)
        {
            _adminViewModel = adminViewModel;
            InitializeComponent();
            DataContext = new UserViewModel();

        }

        private void UserTask(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as UserViewModel;
            if (viewModel != null && viewModel.AddUser(_adminViewModel))
                Close();
        }
    }
}
