﻿using System;
using System.Windows.Controls;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View.Admin
{
    /// <summary>
    /// Interaction logic for UserActions.xaml
    /// </summary>
    public partial class UserActions
    {
        private readonly Guid _userGuid;

        private UserActions()
        {     
        }
        public UserActions(Guid userGuid)
        {
            _userGuid = userGuid;
            InitializeComponent();
            DataContext = new ActionViewModel();
            Loaded += UserActions_Loaded;
        }

        void UserActions_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var context = DataContext as ActionViewModel;
            if (context != null) context.GetActionList(_userGuid);
        }

        private void WatermarkTasksFilter(object sender, TextChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
