﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for ScheduledTaskAbout.xaml
    /// </summary>
    public partial class ScheduledTaskAbout
    {
        private readonly RepresentScheduledTask _scheduledTask;

        public ScheduledTaskAbout(RepresentScheduledTask scheduledTask)
        {
            _scheduledTask = scheduledTask;
            InitializeComponent();
            DataContext = new AboutViewModel();
            Loaded += ScheduledTaskAbout_Loaded;
        }

        void ScheduledTaskAbout_Loaded(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AboutViewModel;
            if (context != null) context.ShowInfo(_scheduledTask);
        }

    }
}
