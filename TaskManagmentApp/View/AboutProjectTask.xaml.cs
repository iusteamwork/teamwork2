﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for AboutProjectTask.xaml
    /// </summary>
    public partial class AboutProjectTask
    {
        private readonly RepresentTask _representTask;
        
        public AboutProjectTask(RepresentTask representTask)
        {
            _representTask = representTask;
            InitializeComponent();
            DataContext = new AboutViewModel();
            Loaded += TaskAbout_Loaded;
        }

        void TaskAbout_Loaded(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AboutViewModel;
            if (context != null) context.ShowProjectTaskInfo(_representTask);
        }
    }
}
