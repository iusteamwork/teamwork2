﻿using System.Windows;
using TaskManagmentApp.Data.RepersentTypes;
using TaskManagmentApp.ViewModels;

namespace TaskManagmentApp.View
{
    /// <summary>
    /// Interaction logic for TaskAbout.xaml
    /// </summary>
    public partial class TaskAbout
    {
        private readonly RepresentTask _representTask;
        
        public TaskAbout(RepresentTask representTask)
        {
            _representTask = representTask;
            InitializeComponent();
            DataContext = new AboutViewModel();
            Loaded += TaskAbout_Loaded;
        }

        void TaskAbout_Loaded(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AboutViewModel;
            if (context != null) context.ShowInfo(_representTask);
        }
    }
}
